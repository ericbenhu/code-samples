#include "GLSL.h"
int main(int ac, char **av) {
    glutInitWindowSize(1, 1);
    glutInitWindowPosition(400, 100);
    glutInit(&ac, av);
    glutCreateWindow(av[0]);
    GLenum err = glewInit();
	if (err != GLEW_OK)
        printf("Error initializaing GLEW: %s\n", glewGetErrorString(err));
    else {
        GLSL_PrintExtensions();
    	GLSL_PrintInfo();
    }
    getchar();
    return 0;
}
