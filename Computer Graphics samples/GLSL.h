/*  =====================================
    GLSL.h - GLSL support
    Copyright (c) Jules Bloomenthal, 2011
    All rights reserved
    =====================================
*/

//****** Print Info

void GLSL_PrintInfo();

void GLSL_PrintExtensions();

void GLSL_PrintProgramLog(int programID);

void GLSL_PrintProgramAttributes(int programID);

void GLSL_PrintProgramUniforms(int programID);


//****** Shader Compilation

int GLSL_CompileShaderViaFile(const char *filename, int type);

int GLSL_CompileShaderViaCode(const char *code, int type);


//****** Program Linking

int GLSL_LinkProgramViaFile(const char *vertexShaderFile, const char *fragmentShaderFile);

int GLSL_LinkProgramViaCode(const char *vertexShaderCode, const char *fragmentShaderCode);

int GLSL_LinkProgram(int vshader, int fshader);
