/*  =====================================
    Camera.cpp - GL view control
    Copyright (c) Jules Bloomenthal, 2012
    All rights reserved
    =====================================
*/

#include "Camera.h"


//========== Initialization ===============

Camera::Camera(int w, int h, float fov, float dolly)
{
	this->fov				= fov;
    screenWidth		= w;
	screenHeight	= h;
    eventMode		= E_None;
    this->dolly			= dolly;
    screenShift[0]	= screenShift[1]	= 0;
	mouseRef[0]		= mouseRef [1]	= 0;
	mouseNow[0]	= mouseNow[1]	= 0;
    for (int i = 0; i < 3; i++)
        rotate[i] = rotateCenter[i] = rotateOffset[i] = 0;
	for (int i = 0; i < 3; i++)
        viewCenter[i] = viewOffset[i] = 0;
}

void Camera::SetFOV(float fov)
{
	this->fov = fov;
}

void Camera::SetSize(int screenWidth, int screenHeight)
{
	this->screenHeight = screenHeight;
	this->screenWidth = screenWidth;
}


//========== View Matrix ===============

// for Perspective specification, near < 0 and far < 0, and far < near
// near/far already defined by MS

float nearPlane = -.01f;
float farPlane = -5;

void CheckNearFar(float &n, float &f)
{
	n = abs(n);
	f = abs(f);
	if (n > f) {
		float temp = n;
		n = f;
		f = temp;
	}
}

mat4 Camera::GetView()
{
	// model_view matrices
	mat4 scale	= Scale(.2f, .2f, .2f);
	mat4 rotx		= RotateX(rotate[0]);
	mat4 roty		= RotateY(rotate[1]);
	mat4 tran		= Translate(screenShift[0], screenShift[1], -dolly);

	// camera matrices	
	float aspect	= (float) screenWidth / (float) screenHeight;
	CheckNearFar(nearPlane, farPlane);
	mat4 persp	= Perspective(fov, aspect, nearPlane, farPlane);

	// compute view matrix
	mat4 view = persp*tran*rotx*roty*scale;
	return view;
}

mat4 Camera::GetPersp()
{
	float aspect	= (float) screenWidth / (float) screenHeight;
	CheckNearFar(nearPlane, farPlane);
	mat4 persp	= Perspective(fov, aspect, nearPlane, farPlane);
	return persp;
}

mat4 Camera::GetModelView()
{
	mat4 scale	= Scale(.2f, .2f, .2f);
	mat4 rotx		= RotateX(rotate[0]);
	mat4 roty		= RotateY(rotate[1]);
	mat4 tran		= Translate(screenShift[0], screenShift[1], -dolly);
	mat4 view		= tran*rotx*roty*scale;
	return view;
}


//========== Mouse ===============

void Camera::MouseUp()
{
    eventMode = E_None;
}

void Camera::MouseDown(int x, int y, bool shft, bool ctrl, bool alt)
{
    mouseNow[0] = mouseRef[0] = x;
    mouseNow[1] = mouseRef[1] = y;
    dollyRef = dolly;
    speed = alt? 0.1f : 0.5f;
    eventMode = 
        !shft && !ctrl?	E_Rotate		:
         shft && !ctrl?	E_Translate	:
        !shft &&  ctrl?	E_Dolly			:
									E_None;
}

void Camera::MouseDrag(int x, int y)
{
	// compute mouse change
    mouseNow[0] = x;
    mouseNow[1] = y;
    int dx = x-mouseRef[0];
    int dy = y-mouseRef[1];

	// respond to event
    if (eventMode == E_Rotate) {
        mouseRef[0] = x;
        mouseRef[1] = y;
        rotate[0] -= speed*dy;
        rotate[1] += speed*dx; // world y-axis points up
    }
    if (eventMode == E_Translate) {
        mouseRef[0] = mouseNow[0];
        mouseRef[1] = mouseNow[1];
		float f = .005f*speed;
        screenShift[0] += f*dx;
        screenShift[1] += f*dy;
    }
    if (eventMode == E_Dolly) {
        float a = -(float) dy/screenHeight;
        if (dollyRef > -.1 && dollyRef < .1)
			// dolly is miniscule so increment rather than scale
            dolly = dollyRef-.2f*a;
        else {
            float f = 9*a+1; // dy > 0? SlowInOut(1., 10., a) : SlowInOut(1., -2., a);
            dolly = f*dollyRef;
        }
    }
}
