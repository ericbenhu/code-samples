/*  =======================================================
    Eric Hu
	Hu_DrawRotatingTriangle.cpp
	Modified DrawRetainedBufferShaders.cpp
	
	DrawRetainedBufferShaders.cpp
    Use vertex buffer and shaders to draw a triangle
    Copyright (c) Jules Bloomenthal, 2012
    All rights reserved
    =======================================================
*/

#include <stdio.h>
#include "glew.h"
#include "freeglut.h"
#include "GLSL.h"
#include "freeglut_ext.h"
#include "freeglut_std.h"
#include <time.h>

#define _USE_MATH_DEFINES
#include <cmath>


//****** Globals

GLuint vBufferID   = -1;

// 12 vertices
float points[][2] = {{-.75f, -.75f},{-.75f, .75f},{.25f, .75f},{.25f, .5f},{-.5f, .5f},{-.5f, .25f},
					{0, .25f},{0, 0},{-.5f, 0},{-.5f, -.5f},{.25f, -.5f},{.25f, -.75f}};
    // vertexShader below does not use gl_Matrix; thus all coordinates here are in
    // clip space (+/- 1) and functions such as glOrtho or glScale have no effect
float colors[][3] = {{ 1, 1, 1}, { 1, 0, 0}, {.5, 0, 0}, {1, 1, 0},  {.5, 1, 0},
{ 0, 1, 0}, { 0, 1, 1}, {0,  0, 1}, {1, 0, 1},  {.5, 0, .5}, {.5, .5, .5}, {0, 0, .5}};

// 9 triangles
int   triangles[][3] = {{0, 1, 8}, {1, 2, 4}, {2, 3, 4}, {1, 4, 8}, {5, 6, 8},
					{6, 7, 8}, {8, 9, 0}, {9, 10, 11}, {9, 11, 0}};
int   nVertsToRender = sizeof(triangles)/sizeof(int);


//****** Trivial Vertex and Fragment (Pixel) Shaders

// the vertex shader accepts as inputs a position and a color pulled from GPU memory
// (accessed via vBufferID); usually at this point the input position is transformed
// by the current transformation matrix; in the present case, however, the inputs are
// passed unchanged to the vertex shader output (using a specified output for color
// and a pre-defined output for position); the resulting output vertices are formed,
// three at a time, into a triangle that is clipped and sent to the rasterizer

char *vertexShader = "\
	#version 130\n\
	in vec4 vPosition;\n\
	in vec3 vColor;\n\
	out vec4 color;\n\
	void main()\n\
	{\n\
		gl_Position = vPosition;\n\
		color = vec4(clamp(vPosition.x, 0, 1), clamp(vPosition.y, 0, 1), 0, 1);\n\
	    // color = vec4(vColor, 1);\n\
	}\n";

// the rasterizer scan-converts the triangle into a series of pixels, interpolating
// the color attributes as it proceeds; this interpolated value is given as input to
// the fragment shader, which passes it unchanged to the pixel

char *fragmentShader = "\
	#version 130\n\
	in vec4 color;\n\
	out vec4 fColor;\n\
	void main()\n\
	{\n\
		fColor = color;\n\
	}\n";


//****** Initializations

void InitVertexBuffer()
{
    // a vertex array consists of one or more vertex buffers residing on the GPU;
    // this routine initializes a vertex array containing a single vertex buffer;
    // the vertex buffer contains a sub-buffer devoted to vertex positions and a
    // second sub-buffer devoted to vertex colors

    // create a "vertex array object" and make it the active vertex array
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // create a vertex buffer for the array, and make it the active vertex buffer
    glGenBuffers(1, &vBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vBufferID);

    // allocate buffer memory to hold vertex locations and colors
    glBufferData(GL_ARRAY_BUFFER, sizeof(points)+sizeof(colors), NULL, GL_STATIC_DRAW);

    // load non-interleaved data to the GPU
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(points), points);
        // start at beginning of buffer, for length of points array
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(points), sizeof(colors), colors);
        // start at end of points array, for length of colors array
}

void InitShader()
{
    // build and use shader program from inline code
	GLuint program = GLSL_LinkProgramViaCode(vertexShader, fragmentShader);
    glUseProgram(program);
        // the  triangles still render if the shader program is disabled
        // for some reason, however, the colors are all set to white
    
    // status report
	GLSL_PrintInfo();
    if (program) {
        GLSL_PrintProgramAttributes(program);
		GLSL_PrintProgramAttributes(program);
	}
    else
        printf("Failed to link shader program\n");

    // those arrays within the vertex buffer that provide input to the vertex
    // shader (ie, position, color) must be connected to the shader; with
    // GLSLv4.0, a layout parameter can be given directly to a shader; pre-v4.0,
    // each named shader input is associated with an array in the vertex buffer
    // with the following attribute calls, after linking the shader program

    // associate position input to shader with position array in vertex buffer 
    GLuint vPosition = glGetAttribLocation(program, "vPosition");
    glEnableVertexAttribArray(vPosition);
    glVertexAttribPointer(vPosition, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);
        // last argument is offset of data within vertex buffer

    // associate color input to shader with color array in vertex buffer
    GLuint vColor = glGetAttribLocation(program, "vColor");
    glEnableVertexAttribArray(vColor);
    glVertexAttribPointer(vColor, 3, GL_FLOAT, GL_FALSE, 0, (void *) sizeof(points));
}

void RotatePoint(float* point, float* rotatedPoint, float angle)
{
	// point[0] is x
	// point[1] is y
	float x = cos(angle)*point[0] - sin(angle)*point[1];
	float y = sin(angle)*point[0] + cos(angle)*point[1];
	rotatedPoint[0] = x;
	rotatedPoint[1] = y;
}

clock_t prevTime = clock();
float angle = 0; // in degrees
float dAngle = 25; // in deg/sec	

void Idle()
{
    // compute elapsed time since last call to Idle
    clock_t now = clock();
    float dt = (float) (now-prevTime)/CLOCKS_PER_SEC;
    prevTime = now;
    // compute new angle of rotation
    angle += dt*dAngle / 10;
    // allocate temporary array for rotated points
    const int nPoints = sizeof(points)/sizeof(float);
    float rotatedPoints[nPoints][2];
    // rotate the points
    for (int i = 0; i < nPoints; i++)
        RotatePoint(points[i], rotatedPoints[i], angle);
    // update points to GPU
    glBindBuffer(GL_ARRAY_BUFFER, vBufferID);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(points), rotatedPoints);
    // redisplay
    glutPostRedisplay();
} 

bool paused = false;

void Keyboard(unsigned char key, int x, int y)
{
	if (key == 27)    // ESC: exit program
	    glutLeaveMainLoop();
	if (key == 'p') { // pause/play
	    paused = !paused;
	    prevTime = clock();
	    glutIdleFunc(paused? NULL : Idle);
	}
	if (key == 'r')   // reverse rotation
	    dAngle = -dAngle;
}



//****** Application

void Display()
{
	// glDrawArrays(GL_TRIANGLES, 0, 3); (glDrawElements scales better)
    // clear background to grey
    glClearColor(.5, .5, .5, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    // draw triangles, sending vertices to shader according to triangle indices
    glDrawElements(GL_TRIANGLES, nVertsToRender, GL_UNSIGNED_INT, triangles);
    glFlush();
}

void Close()
{
	// unbind vertex buffer and free GPU memory
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &vBufferID);
}

void main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitWindowSize(400, 400);
    glutCreateWindow("Shader Example");
    glewInit();
    InitVertexBuffer();
    InitShader();
    glutDisplayFunc(Display);
	glutIdleFunc(Idle);
	glutKeyboardFunc(Keyboard);
    glutCloseFunc(Close);
    glutMainLoop();
}
