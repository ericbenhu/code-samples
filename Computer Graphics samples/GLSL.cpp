/*  =====================================
    GLSL.cpp - GLSL support
    Copyright (c) Jules Bloomenthal, 2011
    All rights reserved
    =====================================
*/

#pragma warning(disable : 4224) // else complaint about GLsync

#include <vector>
#include "GLSL.h"
#include "gl/glew.h"

// perhaps part or all of this should be in a namespace


//****** Print OpenGL and GLSL Details

void GLSL_PrintInfo()
{
    // print OpenGL/GLSL info
    const GLubyte *renderer    = glGetString(GL_RENDERER);
    const GLubyte *vendor      = glGetString(GL_VENDOR);
    const GLubyte *version     = glGetString(GL_VERSION);
    const GLubyte *glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
    GLint major, minor;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);
    printf("GL vendor: %s\n", vendor);
    printf("GL renderer: %s\n", renderer);
    printf("GL version: %s\n", version);
 // printf("GL version (integer): %d.%d\n", major, minor);
    printf("GLSL version: %s\n", glslVersion);
}

void GLSL_PrintExtensions()
{
    const GLubyte *extensions = glGetString(GL_EXTENSIONS);
    char *skip = "(, \t\n", buf[100];
    printf("\nGL extensions:\n");
		if (extensions)
			for (char *c = (char *) extensions; *c; ) {
					c += strspn(c, skip);
					int nchars = strcspn(c, skip);
					strncpy(buf, c, nchars);
					buf[nchars] = 0;
					printf("  %s\n", buf);
					c += nchars;
	}
}

void GLSL_PrintProgramLog(int programID)
{
    GLint logLen;
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &logLen);
    if (logLen > 0) {
        char *log = new char[logLen];
        GLsizei written;
        glGetProgramInfoLog(programID, logLen, &written, log);
        printf("Program log:\n%s", log);
        delete [] log;
    }
}

void GLSL_PrintProgramAttributes(int programID)
{
    GLint maxLength, nAttribs;
    glGetProgramiv(programID, GL_ACTIVE_ATTRIBUTES, &nAttribs);
    glGetProgramiv(programID, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxLength);
    char *name = new char[maxLength];
    GLint written, size;
    GLenum type;
    printf("Active Shader Attributes\n  Index  |  Name\n");
    for (int i = 0; i < nAttribs; i++) {
        glGetActiveAttrib(programID, i, maxLength, &written, &size, &type, name);
        GLint location = glGetAttribLocation(programID, name);
        printf("  %-5i  |  %s\n", location, name);
    }
    delete [] name;
}

void GLSL_PrintProgramUniforms(int programID)
{
    GLenum type;
    GLchar name[201];
    GLint nUniforms, length, size;
    glGetProgramiv(programID, GL_ACTIVE_UNIFORMS, &nUniforms);
    printf("%i uniform%s (shader %i)\n", nUniforms, nUniforms == 1? "" : "s", programID);
    for (int i = 0; i < nUniforms; i++) {
        glGetActiveUniform(programID, i,  200,  &length, &size, &type, name);
        printf("  %s\n", name);
    }
}


//****** Shader Compilation

// Angel & Shreiner method in InitShader.cpp very unsafe re memory leak
// here, deallocation is guarranteed by the destructor

int GLSL_CompileShaderViaFile(const char *filename, GLint type)
{
		char *buf;
		FILE* fp = fopen(filename, "r");
		if (fp == NULL)
			return 0;
		fseek(fp, 0L, SEEK_END);
		long size = ftell(fp);
		fseek(fp, 0L, SEEK_SET);
		buf = new char[size+1];
		fread(buf, 1, size, fp);
		buf[size] = '\0';
		fclose(fp);
        delete [] buf;
		return GLSL_CompileShaderViaCode(buf, type);
}

int GLSL_CompileShaderViaCode(const char *code, GLint type)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &code, NULL);
	glCompileShader(shader);
    // check compile status
    GLint result;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE) {
        // report logged errors
        GLint logLen;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);
        if (logLen > 0) {
            GLsizei written;
            char *log = new char[logLen]; // Wolff uses malloc
            glGetShaderInfoLog(shader, logLen, &written, log);
            printf(log);
            delete [] log;
        }
        else
            printf("Shader compilation failed\n");
        return 0;
    }
    const char *t = type == GL_VERTEX_SHADER? "Vertex" : "Fragment";
    printf("%s shader (%i) successfully compiled\n", t, shader);
    return shader;
}


//****** Program Linking

int GLSL_LinkProgramViaFile(const char *vertexShaderFile, const char *fragmentShaderFile)
{
	int vshader = GLSL_CompileShaderViaFile(vertexShaderFile, GL_VERTEX_SHADER);
	int fshader = GLSL_CompileShaderViaFile(fragmentShaderFile, GL_FRAGMENT_SHADER);
	return GLSL_LinkProgram(vshader, fshader);
}

int GLSL_LinkProgramViaCode(const char *vertexShaderCode, const char *fragmentShaderCode)
{
	int vshader = GLSL_CompileShaderViaCode(vertexShaderCode, GL_VERTEX_SHADER);
	int fshader = GLSL_CompileShaderViaCode(fragmentShaderCode, GL_FRAGMENT_SHADER);
	return GLSL_LinkProgram(vshader, fshader);
}

int GLSL_LinkProgram(int vshader, int fshader)
{
    int programID = 0;

    // create shader program
    if (vshader && fshader)
        programID = glCreateProgram();

    if (programID > 0) {

        // attach shaders to program
        glAttachShader(programID, vshader);
        glAttachShader(programID, fshader);

        // link and verify
        glLinkProgram(programID);
        GLint status;
        glGetProgramiv(programID, GL_LINK_STATUS, &status);
        if (status == GL_FALSE)
            GLSL_PrintProgramLog(programID);
    }

    return programID;
}
