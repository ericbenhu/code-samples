#include "Draw.h"
#include <gl/glew.h>
#include <gl/freeglut.h>
#include "GLSL.h"


//****** Errors

int Errors(char *buf)
{
    int nErrors = 0;
    buf[0] = 0;
    for (;;) {
        GLenum n = glGetError();
        if (n == GL_NO_ERROR)
            break;
        sprintf(buf+strlen(buf), "%s%s", !nErrors++? "" : ", ", gluErrorString(n));
    }
    return nErrors;
}

void  CheckGL_Errors(char *msg)
{
	char buf[100];
	if (*msg)
		printf("(%s) ", msg);
	if (Errors(buf))
		printf("GL error(s): %s\n", buf);
	else if (*msg)
		printf("<no GL errors>\n");
}


//****** Screen Mode

mat4 ScreenMode(int width, int height)
{
	mat4 scale = Scale(2.f / (float) width, 2.f / (float) height, 1.);
	mat4 tran = Translate(-1, -1, 0);
	return tran*scale;
}

float ScreenDistSq(int x, int y, vec3 p, mat4 m, int width, int height)
{
	vec4 xp = m*p;
 	float xscreen = ((xp.x/xp.w)+1)*(float) width/2;
	float yscreen = ((xp.y/xp.w)+1)*(float) height/2;
	float dx = x-xscreen, dy = y-yscreen;
    return dx*dx+dy*dy;
}

void ScreenLine(float xscreen, float yscreen, mat4 &modelview, mat4 &persp, float p1[], float p2[])
{
    // compute 3D world space line, given by p1 and p2, that transforms
    // to a line perpendicular to the screen at (xscreen, yscreen)
	// m is the full transformation matrix (ie, modelView*projection)
	int vp[4];
	double tpersp[4][4], tmodelview[4][4], a[3], b[3];
	// get viewport
	glGetIntegerv(GL_VIEWPORT, vp);
	// create transposes
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++) {
			tmodelview[i][j] = modelview[j][i];
			tpersp[i][j] = persp[j][i];
		}
	if (gluUnProject(xscreen, yscreen, .25, (const double*) tmodelview, (const double*) tpersp, vp, &a[0], &a[1], &a[2]) == GL_FALSE)
        printf("UnProject false\n");
	if (gluUnProject(xscreen, yscreen, .50, (const double*) tmodelview, (const double*) tpersp, vp, &b[0], &b[1], &b[2]) == GL_FALSE)
        printf("UnProject false\n");
	for (int i = 0; i < 3; i++) {
		p1[i] = static_cast<float>(a[i]);
		p2[i] = static_cast<float>(b[i]);
	}
}


//****** Lines

void Line(float *p1, float *p2, float *col)
{
	// align vertex data
	float points[][3] = {{p1[0], p1[1], p1[2]}, {p2[0], p2[1], p2[2]}};
	float colors[][3]  = {{col[0], col[1], col[2]}, {col[0], col[1], col[2]}};

    // create a vertex buffer for the array, and make it the active vertex buffer
	GLuint vBuf;
    glGenBuffers(1, &vBuf);
    glBindBuffer(GL_ARRAY_BUFFER, vBuf);

    // allocate buffer memory and load location and color data
	glBufferData(GL_ARRAY_BUFFER, sizeof(points)+sizeof(colors), NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(points), points);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(points), sizeof(colors), colors);

	// determine current shader
	int shader;
	glGetIntegerv(GL_CURRENT_PROGRAM, &shader);

    // connect shader input position
	GLuint position = glGetAttribLocation(shader, "position");
    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

    // connect shader input color
    GLuint color = glGetAttribLocation(shader, "color");
    glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, 0, (void *) sizeof(points));

	// draw, cleanup
	glDrawArrays(GL_LINES, 0, 2);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &vBuf);
}

void Line(int x1, int y1, int x2, int y2, float *col)
{
	float p1[] = {(float)x1, (float)y1, 0};
	float p2[] = {(float)x2, (float)y2, 0};
	Line(p1, p2, col);
}

void Rect(int x, int y, int w, int h, float *col)
{
	Line(x, y, x+w, y, col);
	Line(x+w, y, x+w, y+h, col);
	Line(x+w, y+h, x, y+h, col);
	Line(x, y+h, x, y, col);
}


//****** Miscellany

float Random(float min, float max)
{
	return min+((float)(rand()%1000)/1000.f)*(max-min);
}


//****** Points

void Cross(float p[], float s, float col[])
{
	float p1[3], p2[3];
	for (int n = 0; n < 3; n++)
		for (int i = 0; i < 3; i++) {
			float offset = i==n? s : 0;
			p1[i] = p[i]+offset;
			p2[i] = p[i]-offset;
			Line(p1, p2, col);
		}
}

void Asterisk(float p[], float s, float col[])
{
	float p1[3], p2[3];
	for (int i = 0; i < 8; i++) {
		p1[0] = p[0]+(i<4? -s : s);
		p1[1] = p[1]+(i%2? -s : s);
		p1[2] = p[2]+((i/2)%2? -s : s);
		for (int k = 0; k < 3; k++)
			p2[k] = 2*p[k]-p1[k];
		Line(p1, p2, col);
	}
}


//****** Dots

//void Dot(float *point,  float *color)
//{
//	// or, via shaders - create a vertex buffer
//	GLuint vBuf;
//	glGenBuffers(1, &vBuf);
//	glBindBuffer(GL_ARRAY_BUFFER, vBuf);
//	// allocate buffer memory and load location and color data
//	glBufferData(GL_ARRAY_BUFFER, 6*fSize, NULL, GL_STATIC_DRAW);
//	glBufferSubData(GL_ARRAY_BUFFER, 0, 3*fSize, point);
//	glBufferSubData(GL_ARRAY_BUFFER, 3*fSize, 3*fSize, color);
//	// connect buffer to shader inputs
//	glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);
//	glVertexAttribPointer(vColor, 3, GL_FLOAT, GL_FALSE, 0, (void *) (3*fSize));
//	// draw, cleanup
//	glDrawArrays(GL_POINTS, 0, 1);
//	glBindBuffer(GL_ARRAY_BUFFER, 0);
//	glDeleteBuffers(1, &vBuf);
// }

/* void Dot(float *point,  float *color) {
	// disable shader to use native texture-mapping
	int currentProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgram);
	glUseProgram(0);
	// display smooth dot with four lines of code
	glColor3fv(color);
	glBegin(GL_POINTS);
	vec4 pt(point[0], point[1], point[2], 1);
	vec4 xpt = view*pt;
	glVertex2f(pt.x/pt.w, pt.y/pt.w);
	// glVertex3fv(point);
	glEnd();
	// re-enable shader program
	glUseProgram(currentProgram); */

void DotDraw::Initialize() {
	char *vShader = "\
		#version 130\n\
		uniform mat4 view;\n\
		in vec3 position;\n\
		in vec3 color;\n\
		out vec4 vColor;\n\
		void main() {\n\
			vec4 xp = view*vec4(position, 1);\n\
			gl_Position = xp; // /xp.w;\n\
			vColor = vec4(color, 1);\n\
		}\n";
	char *fShader = "\
		#version 130\n\
		in vec4 vColor;\n\
		out vec4 fColor;\n\
		void main() {\n\
			fColor = vColor;\n\
		}\n";
	// initialize shader program
	programID = GLSL_LinkProgramViaCode(vShader, fShader);
	// set, enable, and connect attribute locations for position
	positionID = glGetAttribLocation(programID, "position");
	glEnableVertexAttribArray(positionID);
	// set, enable, and connect attribute locations for color
	colorID = glGetAttribLocation(programID, "color");
	glEnableVertexAttribArray(colorID);
	// set uniform view matrix location
	viewID = glGetUniformLocation(programID, "view");
	// create a vertex buffer for the array, and make it the active vertex buffer
	glGenBuffers(1, &vbufferID);
	glBindBuffer(GL_ARRAY_BUFFER, vbufferID);
	// allocate buffer memory to hold vertex location and color
	int nbytes = 6*sizeof(float);
	glBufferData(GL_ARRAY_BUFFER, nbytes, NULL, GL_STATIC_DRAW);
	// setup texture mapping (single vertex will be treated as 'sprite', rendered as a quad
	glGenTextures(1, &textureID);
	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
	glTexEnvi(GL_POINT_SPRITE, GL_COORD_REPLACE, GL_TRUE);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, 16, 16, 0, GL_RGBA, GL_UNSIGNED_BYTE, GetDiskMatte());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}

void DotDraw::Draw(float *point,  float *color, mat4 &view)
{
	if (programID == -1)
		Initialize();
	// save current shader program
	glGetIntegerv(GL_CURRENT_PROGRAM, &prevProgID);
	glUseProgram(programID);
	// load location and color data to the buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbufferID);
	glBufferSubData(GL_ARRAY_BUFFER, 0, 3*sizeof(float), point);
	glBufferSubData(GL_ARRAY_BUFFER, 3*sizeof(float), 3*sizeof(float), color);
	// set view
	glUniformMatrix4fv(viewID, 1, TRUE, (float *) view);
	// connect vBuffer to vertex shader inputs
	int pointSize = 3*sizeof(float);
	glVertexAttribPointer(positionID, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);
    glVertexAttribPointer(colorID, 3, GL_FLOAT, GL_FALSE, 0, (void *) pointSize);
	// draw
	glDrawArrays(GL_POINTS, 0, 1);
	// re-enable shader program
	glUseProgram(prevProgID);
}

DotDraw::~DotDraw() {
	if (programID >= 0) {
		// glBindBuffer(GL_ARRAY_BUFFER, 0);
		// glDeleteBuffers(1, &vbufferID);
	}
}


 //****** Disk Matte

unsigned char diskMatte[16][16][4]; // raster of 4-byte rgba
unsigned char *diskMatteSet = NULL;

unsigned char diskAlpha[16][16] = {
    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   1,  26,  92, 139, 139,  92,  26,   1,   0,   0,   0,   0,
    0,   0,   0,  11, 139, 241, 254, 255, 255, 254, 241, 139,  11,   0,   0,   0,
    0,   0,  11, 184, 254, 255, 255, 255, 255, 255, 255, 254, 184,  11,   0,   0,
    0,   1, 139, 254, 255, 255, 255, 255, 255, 255, 255, 255, 254, 139,   1,   0,
    0,  26, 241, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 241,  26,   0,
    0,  92, 254, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 254,  92,   0,
    0, 139, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 139,   0,
    0, 139, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 139,   0,
    0,  92, 254, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 254,  92,   0,
    0,  26, 241, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 241,  26,   0,
    0,   1, 139, 254, 255, 255, 255, 255, 255, 255, 255, 255, 254, 139,   1,   0,
    0,   0,  11, 184, 254, 255, 255, 255, 255, 255, 255, 254, 184,  11,   0,   0,
    0,   0,   0,  11, 139, 241, 254, 255, 255, 254, 241, 139,  11,   0,   0,   0,
    0,   0,   0,   0,   1,  26,  92, 139, 139,  92,  26,   1,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0};

unsigned char *GetDiskMatte()
{
	if (!diskMatteSet) {
		unsigned char *a = (unsigned char *) diskAlpha;
		unsigned char *m = (unsigned char *) diskMatte;
		unsigned char white[] = {255, 255, 255};
		for (int i = 0; i < 16*16; i++) {
			memcpy(m, white, 3);
			m += 3;
			*m++ = *a++;
		}
		diskMatteSet = m;
	}
	return diskMatteSet;
}


/****** Wayside

// display matrix as text
//	char buf[100];
//	for (int row = 0; row < 4; row++)
//	for (int col = 0; col < 4; col++) {
//		sprintf(buf, "%3.1f", view[col][row]); // transpose
//		glRasterPos2f( -.9f+col*.15f, .9f-row*.15f);
//		glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (unsigned char*) buf);
//	}

// draw frame
//	ScreenMode(view);
//  glUniformMatrix4fv(viewID, 1, false, (float *) view);
//	float p1[] = {5, 5, 0}, p2[]= {(float) winSize-5, (float) winSize-5, 0};
//	Line(p1, p2, blk);


/****** Matrix Operations

// compute view transformation
// Identity(view);
// for (int i = 0; i < 3; i++)
//      Rotate(view, angle[i], i);
// Translate(view, 0, 0, -2);
// Perspective(view, 22.5f, 1.f, 10.f, -10.f);
// Translate(view, -center[0], -center[1], -center[2]);
// Translate(view, center[0], center[1], center[2]);

void Multiply(float a[][4], float b[][4], float result[][4])
{
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++) {
			float d = 0;
			for (int k = 0; k < 4; k++)
				d += a[i][k]*b[k][j];
			result[i][j] = d;
		}
}

void Concatenate(float a[][4], float b[][4])
{
    float temp[4][4];
    Multiply(a, b, temp);
    memcpy(a, temp, sizeof(temp));
}

void Rotate(float m[][4], float deg, int axis)
{
    float rad = 3.141592f*deg/180.f;
    float c = cos(rad), s = sin(rad);
    int i0 = (axis+2)%3, i1 = (axis+1)%3;
    float temp[4][4];
    Identity(temp);
    temp[i0][i0] = temp[i1][i1] = c;
    temp[i1][i0] = -(temp[i0][i1] = s);
    Concatenate(m, temp);
}

void Scale(float m[][4], float s)
{
	float temp[4][4];
	Identity(temp);
	temp[0][0] = temp[1][1] = temp[2][2] = s;
	Concatenate(m, temp);
}

void Translate(float m[][4], float dx, float dy, float dz)
{
    float temp[4][4];
    Identity(temp);
	temp[3][0] = dx;
	temp[3][1] = dy;
	temp[3][2] = dz;
    Concatenate(m, temp);
}

void Transpose(float m[][4])
{
	for (int row = 0; row < 4; row++)
		for (int col = 0; col < row; col++) {
			float temp = m[row][col];
			m[row][col] = m[col][row];
			m[col][row] = temp;
		}
}

void Perspective(float m[][4], const GLfloat fovy, const GLfloat aspect,
		  const GLfloat zNear, const GLfloat zFar)
{
	float temp[4][4];
	GLfloat fovRad = 3.141592f*fovy/180.f;
    GLfloat top   = tan(fovRad/2) * zNear;
    GLfloat right = top * aspect;
	Identity(temp);
    temp[0][0] = zNear/right;
    temp[1][1] = zNear/top;
    temp[2][2] = -(zFar + zNear)/(zFar - zNear);
	temp[2][3] = -2.0f*zFar*zNear/(zFar - zNear);
    temp[3][2] = -1.0f;
    Concatenate(m, temp);
}*/
