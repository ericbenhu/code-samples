/* ======================================
   Mesh.cpp - basic mesh representation and IO
   Copyright (c) Jules Bloomenthal, Seattle, 2012
   All rights reserved
   ====================================== */

#include "Mesh.h"
#include <iostream>

bool ReadWord(char* &ptr, char *word, int charLimit) {
	ptr += strspn(ptr, " \t");					// skip white space
	int nChars = strcspn(ptr, " \t");	// get # non-white-space characters
	if (!nChars)
		return false;									// no non-space characters
	int nRead = charLimit-1 < nChars? charLimit-1 : nChars;
	strncpy(word, ptr, nRead);
	word[nRead] = 0;							// strncpy does not null terminate
	ptr += nChars;
		return true;
}

void MinMax(std::vector<vec3> &vertices, vec3 &min, vec3 &max)
{
	min[0] = min[1] = min[2] = FLT_MAX;
	max[0] = max[1] = max[2] = -FLT_MAX;
	for (int i = 0; i < (int) vertices.size(); i++) {
		vec3 &v = vertices[i];
		for (int k = 0; k < 3; k++) {
			if (v[k] < min[k])
				min[k] = v[k];
			if (v[k] > max[k])
				max[k] = v[k];
		}
	}
}

void Normalize(std::vector<vec3> &vertices)
{
	vec3 min, max;
	MinMax(vertices, min, max);
	vec3 center(.5f*(min[0]+max[0]), .5f*(min[1]+max[1]), .5f*(min[2]+max[2]));
	float maxrange = 0;
	for (int k = 0; k < 3; k++)
		if ((max[k]-min[k]) > maxrange)
			maxrange = max[k]-min[k];
	float s = 2.f/maxrange;
	for (int i = 0; i < (int) vertices.size(); i++) {
		vec3 &v = vertices[i];
		for (int k = 0; k < 3; k++)
			v[k] = s*(v[k]-center[k]);
	}
}

bool ReadAsciiObj(char *filename,
									std::vector<vec3>	&vertices,
									std::vector<int3>		&triangles,
									std::vector<vec3>	*vertexNormals,
									std::vector<vec2>	*vertexTextures,
									std::vector<int>		*triangleGroups)
{
	// read 'object' file (Alias/Wavefront .obj format); return true if successful;
	// polygons are assumed simple (ie, no holes and not self-intersecting);
	// some file attributes are not supported by this implementation;
	// obj format indexes vertices from 1
	FILE *in = fopen(filename, "r");
	if (!in)
		return false;
	vec2 t;
	vec3 v;
	int group = 0;
	static const int LineLim = 1000, WordLim = 100;
	char line[LineLim], word[WordLim];
	for (int lineNum = 0;; lineNum++) {
		if (feof(in))                            // hit end of file
			break;
		fgets(line, LineLim, in);               // \ line continuation not supported
		if (strlen(line) >= LineLim-1) {         // getline reads LineLim-1 max
			printf("line %d too long", lineNum);
			return false;
		}
		char *ptr = line;
		if (!ReadWord(ptr, word, WordLim))
			continue;
		else if (*word == '#')
			continue;
		else if (!_stricmp(word, "g"))
			// this implementation: group field significant only if integer
			// .obj format, however, supported arbitrary string identifier
			sscanf(ptr, "%d", &group);
		else if (!_stricmp(word, "v")) {         // read vertex coordinates
			if (sscanf(ptr, "%g%g%g", &v.x, &v.y, &v.z) != 3) {
				printf("bad line %d in object file", lineNum);
				return false;
			}
			vertices.push_back(vec3(v.x, v.y, v.z));
		}
		else if (!_stricmp(word, "vn")) {        // read vertex normal
			if (sscanf(ptr, "%g%g%g", &v.x, &v.y, &v.z) != 3) {
				printf("bad line %d in object file", lineNum);
				return false;
			}
			if (vertexNormals)
				vertexNormals->push_back(vec3(v.x, v.y, v.z));
		}
		else if (!_stricmp(word, "vt")) {        // read vertex texture
			if (sscanf(ptr, "%g%g", &t.x, &t.y) != 2) {
				printf("bad line in object file");
				return false;
			}
			if (vertexTextures)
				vertexTextures->push_back(vec2(t.x, t.y));
		}
		else if (!_stricmp(word, "f")) {         // read triangle or polygon
			static std::vector<int> vids;
			vids.resize(0);
			while (ReadWord(ptr, word, WordLim)) { // read arbitrary # face vid/tid/nid        
				// set texture and normal pointers to preceding /
				char *tPtr = strchr(word+1, '/');    // pointer to /, or null if not found
				char *nPtr = tPtr? strchr(tPtr+1, '/') : NULL;
				// use of / is optional (ie, '3' is same as '3/3/3')
				// convert to vid, tid, nid indices (vertex, texture, normal)
				int vid = atoi(word);
				int tid = tPtr && *++tPtr != '/'? atoi(tPtr) : vid;
				int nid = nPtr && *++nPtr != 0? atoi(nPtr) : vid;
				// standard .obj is indexed from 1, mesh indexes from 0
				vid--;
				tid--;
				nid--;
				if (vid < 0 || tid < 0 || nid < 0) { // atoi = 0 is conversion failure
					printf("bad format on line %d\n", lineNum);
					return false;
				}
				vids.push_back(vid);
				// ignore tid, set nid for corresponding vertex
			}
			if (vids.size() == 3) {
				// create triangle
				triangles.push_back(int3(vids[0], vids[1], vids[2]));
				if (triangleGroups)
					triangleGroups->push_back(group);
			}
			else {
				// create polygon as nvids-2 triangles
				int nids = vids.size();
				for (int i = 1; i < nids-1; i++) {
					triangles.push_back(int3(vids[0], vids[i], vids[(i+1)%nids]));
					if (triangleGroups)
						triangleGroups->push_back(group);
				}
			}
		}
		else if (*word == 0 || *word== '\n')                     // skip blank line
			continue;
		else {                                     // unrecognized attribute
			// printf("unsupported attribute in object file: %s", word);
			continue; // return false;
		}
	} // end read til end of file
	return true;
} // end ReadObj

//bool ReadASCII_Obj(char *filename,
//									std::vector<vec3> &vertices,
//									std::vector<int3> &faces)
//{
//	// read 'object' file (Alias/Wavefront .obj ASCII format); return true if successful
//	// this implementation supports only 3D vertices and triangles
//	// triangles are indexed are numbered from 1 in files, but converted to zero-indexing
//	FILE *f = fopen(filename, "r");
//	static const int LineLim = 1000, WordLim = 100;
//	char line[LineLim+1], word[WordLim+1];
//	if (!f)
//		return false;
//	vertices.resize(0);
//	faces.resize(0);
//	while (fgets(line, LineLim, f)) {
//		char *ptr = line;
//		if (!ReadWord(ptr, word, WordLim))
//			continue;
//		if (!_stricmp(word, "v")) {         // read vertex coordinates
//			vec3 v;
//			if (sscanf(ptr, "%g%g%g", &v[0], &v[1], &v[2]) == 3)
//				vertices.push_back(v);
//		}
//		if (!_stricmp(word, "f")) {         // read vertex coordinates
//			int i1, i2, i3;
//			if (sscanf(ptr, "%i%i%i", &i1, &i2, &i3) == 3)
//				faces.push_back(int3(i1-1, i2-1, i3-1));
//		}
//	}
//	return true;
//}
