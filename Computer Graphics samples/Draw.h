#include <gl/glew.h>
#include <gl/freeglut.h>
#include "mat.h"

int Errors(char *buf);
void  CheckGL_Errors(char *msg = NULL);

mat4 ScreenMode(int width, int height);
	// create transformation matrix that maps from (-1,-1),(1,1) to (0,0),(width,height)

void ScreenLine(float xscreen, float yscreen, mat4 &modelview, mat4 &persp, float p1[], float p2[]);
    // compute 3D world space line, given by p1 and p2, that transforms
    // to a line perpendicular to the screen at (xscreen, yscreen)

float ScreenDistSq(int x, int y, vec3 p, mat4 m, int width, int height);
	// return distance squared, in pixels, between screen point (x, y) and
	// point p transformed by view matrix m

float Random(float min, float max);

void Line(float *p1, float *p2, float *col);
void Line(int x1, int y1, int x2, int y2, float *col);

void Rect(int x, int y, int w, int h, float *col);

void Cross(float p[], float s, float col[]);

void Asterisk(float p[], float s, float col[]);
	// draw an asterisk at 3D point p, with scale s and given color

class DotDraw {
public:
	GLint prevProgID;
	GLint programID;									// shader program 
	GLuint positionID, colorID, viewID;	// buffer locations
	GLuint vbufferID;									// vertex buffer
	GLuint textureID;
	DotDraw() {
		prevProgID	=
		programID		=
		positionID		=
		colorID			=
		viewID			=
		vbufferID		=
		textureID		= -1;
	}
	~DotDraw();
	void Initialize();
	void Draw(float *point,  float *color, mat4 &view);
};

unsigned char *GetDiskMatte();
