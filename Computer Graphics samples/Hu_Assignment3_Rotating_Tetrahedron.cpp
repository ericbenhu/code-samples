/*  =======================================================
    Eric Hu
	Hu_Assignment3_Rotate_Tetrahedron

	Draw and rotate a tetrahedron.
    =======================================================
*/

#include <stdio.h>
#include "glew.h"
#include "freeglut.h"
#include "GLSL.h"
#include <time.h>

#define _USE_MATH_DEFINES
#include <cmath>

//****** Globals

GLuint vBufferID   = -1;
GLuint viewID = -1;

// 4 vertices
float points[][3] = {{-.25f,-.25f,0}, {.25f,0,0}, {0, .25f ,0}, {0, 0, .5f}, {0, 0, -.5f}};

    // vertexShader below does not use gl_Matrix; thus all coordinates here are in
    // clip space (+/- 1) and functions such as glOrtho or glScale have no effect

float colors[][3] = {{1, 1, 1}, {1, 0, 0}, {.5, 0, 0}, {1, 1, 0}};

// 6 triangles
int triangles[][3] = {{1,2,3},{0,1,3},{0,2,3},{0,1,4}, {0,2,4},{1,2,4}};

int   nVertsToRender = sizeof(triangles)/sizeof(int);


//****** Trivial Vertex and Fragment (Pixel) Shaders

// the vertex shader accepts as inputs a position and a color pulled from GPU memory
// (accessed via vBufferID); usually at this point the input position is transformed
// by the current transformation matrix; in the present case, however, the inputs are
// passed unchanged to the vertex shader output (using a specified output for color
// and a pre-defined output for position); the resulting output vertices are formed,
// three at a time, into a triangle that is clipped and sent to the rasterizer

char *vertexShader = "\
	#version 130\n\
	in vec4 vPosition;\n\
	in vec3 vColor;\n\
	out vec4 color;\n\
	uniform mat3 view;\n\
	void main()\n\
	{\n\
		gl_Position = vec4(view*vPosition.xyz, 1);\n\
		//color = vec4(clamp(vPosition.x, 0, 1), clamp(vPosition.y, 0, 1), 0, 1);\n\
	    color = vec4(vColor, 1);\n\
	}\n";

// the rasterizer scan-converts the triangle into a series of pixels, interpolating
// the color attributes as it proceeds; this interpolated value is given as input to
// the fragment shader, which passes it unchanged to the pixel

char *fragmentShader = "\
	#version 130\n\
	in vec4 color;\n\
	out vec4 fColor;\n\
	void main()\n\
	{\n\
		fColor = color;\n\
	}\n";


//****** Initializations

void InitVertexBuffer()
{
    // a vertex array consists of one or more vertex buffers residing on the GPU;
    // this routine initializes a vertex array containing a single vertex buffer;
    // the vertex buffer contains a sub-buffer devoted to vertex positions and a
    // second sub-buffer devoted to vertex colors

    // create a "vertex array object" and make it the active vertex array
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // create a vertex buffer for the array, and make it the active vertex buffer
    glGenBuffers(1, &vBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vBufferID);

    // allocate buffer memory to hold vertex locations and colors
    glBufferData(GL_ARRAY_BUFFER, sizeof(points)+sizeof(colors), NULL, GL_STATIC_DRAW);

    // load non-interleaved data to the GPU
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(points), points);
        // start at beginning of buffer, for length of points array
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(points), sizeof(colors), colors);
        // start at end of points array, for length of colors array
}

void InitShader()
{
    // build and use shader program from inline code
	GLuint program = GLSL_LinkProgramViaCode(vertexShader, fragmentShader);

    glUseProgram(program);
        // the  triangles still render if the shader program is disabled
        // for some reason, however, the colors are all set to white
    
    // status report
	GLSL_PrintInfo();
    if (program) {
        GLSL_PrintProgramAttributes(program);
		GLSL_PrintProgramAttributes(program);
	}
    else
        printf("Failed to link shader program\n");

	// get access to the uniform variable
	viewID = glGetUniformLocation(program, "view");
		// need be set only once, after the shaders are linked

    // those arrays within the vertex buffer that provide input to the vertex
    // shader (ie, position, color) must be connected to the shader; with
    // GLSLv4.0, a layout parameter can be given directly to a shader; pre-v4.0,
    // each named shader input is associated with an array in the vertex buffer
    // with the following attribute calls, after linking the shader program

    // associate position input to shader with position array in vertex buffer 
    GLuint vPosition = glGetAttribLocation(program, "vPosition");
    glEnableVertexAttribArray(vPosition);
    glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);
        // last argument is offset of data within vertex buffer

    // associate color input to shader with color array in vertex buffer
    GLuint vColor = glGetAttribLocation(program, "vColor");
    glEnableVertexAttribArray(vColor);
    glVertexAttribPointer(vColor, 3, GL_FLOAT, GL_FALSE, 0, (void *) sizeof(points));

	glBindBuffer(GL_ARRAY_BUFFER, vBufferID);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(points), points);
}

void Identity(float view[][3])
{
	// Identity matrix based off of the set points
	for(int i = 0; i < 3; i++)
		for(int j = 0; j < 3; j++)
			if(i == j)
				view[i][j] = 1;
			else
				view[i][j] = 0;
}

void RotateX(float point[][3], float angle)
{
	Identity(point);
	// return the matrix required to rotate x
	point[1][1] = cos(angle);
	point[1][2] = -sin(angle);
	point[2][1] = sin(angle);
	point[2][2] = cos(angle);
}

void RotateY(float point[][3], float angle)
{
	Identity(point);
	// return the matrix required to rotate y
	point[0][0] = cos(angle);
	point[0][2] = sin(angle);
	point[2][0] = -sin(angle);
	point[2][2] = cos(angle);
}

void RotateZ(float point[][3], float angle)
{
	Identity(point);
	// return the matrix required to rotate z
	point[0][0] = cos(angle);
	point[0][1] = -sin(angle);
	point[1][0] = sin(angle);
	point[1][1] = cos(angle);
}

void Multiply(float a[][3], float b[][3], float result[][3])
{
	// matrix multiply a to b
	for(int i = 0; i < 3; i++)
		for(int j = 0; j < 3; j++)
			result[i][j] = a[i][0]*b[0][j] + a[i][1]*b[1][j] + a[i][2]*b[2][j];
}

void Concatenate(float a[][3], float b[][3])
{
    // a = a*b -- multiply 3x3 matrix a by b, results to a
    float temp[3][3];
    Multiply(a, b, temp);
    memcpy(a, temp, sizeof(temp));
}

clock_t prevTime = clock();
float angle[3] = {0,0,0};
float dAngle[3] = {1, 1, 1}; // in deg/sec	

void Idle()
{
    // compute elapsed time since last call to Idle
    clock_t now = clock();
    float dt = (float) (now-prevTime)/CLOCKS_PER_SEC;
    prevTime = now;

    // allocate temporary array for rotated points

	float view[3][3];  // view matrix
	float temp[3][3];  // temporary matrix

	// update the angles of rotation
	for (int i = 0; i < 3; i++)
		angle[i] += dt*dAngle[i];

	// initialize view to identity
	Identity(view);

	// set temp matrix to be rotation around X-axis
	RotateX(temp, angle[0]);

	// concatenate the rotation into the view
	Concatenate(view, temp);

	// repeat for Y-axis and Z-axis rotations
	RotateY(temp, angle[1]);
	Concatenate(view, temp);
	RotateZ(temp, angle[2]);
	Concatenate(view, temp);

	// update the view matrix
	glUniformMatrix3fv(viewID, 1, false, (float *) view);

 //   // update points to GPU
    //glBindBuffer(GL_ARRAY_BUFFER, vBufferID);
    //glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(points), rotatedPoints);

    // redisplay
    glutPostRedisplay();
} 

bool paused = false;

void Keyboard(unsigned char key, int x, int y)
{
	if (key == 27)    // ESC: exit program
	    glutLeaveMainLoop();
	if (key == 'p') { // pause/play
	    paused = !paused;
	    prevTime = clock();
	    glutIdleFunc(paused? NULL : Idle);
	}
	if (key == 'r')   // reverse rotation
		for(int i = 0; i < 3; i++)
			dAngle[i] = -dAngle[i];
}



//****** Application

void Display()
{
    // clear background to grey
    glClearColor(.5, .5, .5, 1);
    glClear(GL_COLOR_BUFFER_BIT);

	
	// enable depth test
	glEnable(GL_DEPTH_TEST);
	glClear(GL_DEPTH_BUFFER_BIT);

    // draw triangles, sending vertices to shader according to triangle indices
    glDrawElements(GL_TRIANGLES, nVertsToRender, GL_UNSIGNED_INT, triangles);
    glFlush();
}

void Close()
{
	// unbind vertex buffer and free GPU memory
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &vBufferID);
}

void main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitWindowSize(400, 400);
    glutCreateWindow("Shader Example");
    glewInit();
    InitVertexBuffer();
    InitShader();
    glutDisplayFunc(Display);
	glutIdleFunc(Idle);
	glutKeyboardFunc(Keyboard);
    glutCloseFunc(Close);
    glutMainLoop();
}
