/*  =====================================
    Camera.h - mouse, camera
    Copyright (c) Jules Bloomenthal, 2012
    All rights reserved
    ===================================== */

#ifndef CAMERA_HDR
#define CAMERA_HDR

#include "mat.h"

//  Mouse-driven orientation and scale of orthographic or perspective OpenGL view
//      LEFT mouse: rotate
//      with SHIFT: translate
//      with CTRL:  zoom
//      with ALT:   slow

class Camera
{
public:
    Camera(int screenWidth, int screenHeight, float fov, float dolly = -5);

	void SetFOV(float fov);

	void SetSize(int screenWidth, int screenHeight);

	mat4 GetView();
        // get view matrix based on camera settings
	mat4 GetPersp();
	mat4 GetModelView();

    void MouseUp();

    void MouseDown(int x, int y, bool shft, bool ctrl, bool alt);

    void MouseDrag(int x, int y);
        // redraw if returns true

	float GetDolly() {
		return dolly;
	}

private:
    enum Event {E_Rotate = 0, E_Translate, E_Dolly, E_None};

    // view variables
	int		screenWidth;
    int		screenHeight;
    // orthographic
    float	screenShift[2];		// screen space translation
    // perspective
	float	fov;
    float	dolly;
    float	dollyRef;
    // rotation
	float	rotate[3];				// rotation in degrees about x, y, and z axes
    float	rotateCenter[3];	// world rotation origin
    float	rotateOffset[3];	// for temp change in world rotation origin
    // translation
    float	viewCenter[3];		// camera look-at location
    float	viewOffset[3];		// to effect temporary change in look-at location

    // mouse state
    Event	eventMode;
    int		mouseNow[2];		// current mouse location
    int		mouseRef[2];			// mouse location on mouse-down
    float	speed;					// fast/slow for rotation/translation

    // operations
    void GetModelRotate(double m[][4]);
};

#endif
