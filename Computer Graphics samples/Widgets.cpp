/*  =====================================
    Widgets.cpp - GL view control
    Copyright (c) Jules Bloomenthal, 2012
    All rights reserved
    ===================================== */

#include "Draw.h"
#include "Widgets.h"
#include <gl/freeglut.h>


//****** Buttons

Button::Button(int x, int y, int w, int h) :
	x(x), y(y), w(w), h(h) {
	winW = -1;
}

void Button::Draw(char *name, float *color) {
	if (winW < 0) {
		winW = glutGet(GLUT_WINDOW_WIDTH);
		winH = glutGet(GLUT_WINDOW_HEIGHT);
	}
	Rect(x, y, w, h, color);
	float xPos = (2*(x+5))/(float)winW-1.f;
	float yPos = (2*(y+6))/(float)winH-1.f;
	glRasterPos2f(xPos, yPos);
	void *font = GLUT_BITMAP_HELVETICA_18; // GLUT_BITMAP_TIMES_ROMAN_24
	glutBitmapString(font, (unsigned char*) (name));
}

bool Button::Hit(int x, int y) {
	return	x >= this->x && x <= this->x+w &&
				y >= this->y && y <= this->y+h;
}


//****** Sliders

Slider::Slider(int x, int y, int size, float min, float max, float init) :
	x(x), y(y), size(size), min(min), max(max) {
    loc  = (int) (x+(float)((init-min)/(max-min))*size);
	winW = -1;
}

void Slider::Draw(char *name) {
	float blk[] = {0, 0, 0}, wht[] = {1, 1, 1};
	glLineWidth(1.5f);
	Line(x, y, x+size, y, blk);
	Line(loc, y-10, loc, y+10, wht);
	if (name) {
		if (winW == -1) {
			winW = glutGet(GLUT_WINDOW_WIDTH);
			winH = glutGet(GLUT_WINDOW_HEIGHT);
		}
		float xPos = (2*(x+size+5))/(float)winW-1.f;
		float yPos = (2*(y-5))/(float)winH-1.f;
		char buf[100];
		sprintf(buf, "%s: ", name);
		sprintf(buf+strlen(buf), "%4.2f\n", GetValue());
		glRasterPos2f(xPos, yPos);
		void *font = GLUT_BITMAP_HELVETICA_18; // GLUT_BITMAP_TIMES_ROMAN_24
		glutBitmapString(font, (unsigned char*) buf);
	}
}

float Slider::GetValue() {
    return min+((float)(loc-x)/size)*(max-min);
}

bool Slider::Mouse(int x, int y) {
    bool hit =	x >= this->x && x <= this->x+size &&
						y >= this->y-10 && y <= this->y+10;
	if (hit)
            loc = x;
	return hit;
}


//****** Movers

float DotProduct(float a[], float b[])
{
	return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}

void Mover::SetPlane(int x, int y, mat4 &modelview, mat4 &persp)
{
    ScreenLine((float) x, (float) y, modelview, persp, p1, p2);
        // get two points that transform to pixel x,y
	for (int i = 0; i < 3; i++)
        plane[i] = p2[i]-p1[i];
        // set plane normal to that of line p1p2
    // Normalize(plane); // unneeded
    plane[3] = -DotProduct(plane, point);
        // pass plane through point
}

void Mover::Pick(const float *p, int x, int y, mat4 &modelview, mat4 &persp)
{
    memcpy(point, p, 3*sizeof(float));
    SetPlane(x, y, modelview, persp);
}

void Mover::Drag(int x, int y, mat4 &modelview, mat4 &persp)
{
    float p1[3], p2[3], axis[3];
    ScreenLine(static_cast<float>(x), static_cast<float>(y), modelview, persp, p1, p2);
        // get two points that transform to pixel x,y
    for (int i = 0; i < 3; i++)
        axis[i] = p2[i]-p1[i];
        // direction of line through p1
    float pdDot = DotProduct(axis, plane);
        // project onto plane normal
    float a = (-plane[3]-DotProduct(p1, plane))/pdDot;
    for (int j = 0; j < 3; j++)
        point[j] = p1[j]+a*axis[j];
        // intersection of line with plane
}
