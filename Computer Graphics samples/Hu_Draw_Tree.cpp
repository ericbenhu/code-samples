/*  =======================================================
    Hu_Draw_Tree.cpp
	Author: Eric Hu

	Description:
	This program will algorithmically draw a 3 dimensional tree.
    =======================================================
*/

#include <time.h>
#include <gl/glew.h>
#include <gl/freeglut.h>
#include "mat.h"
#include "Draw.h"
#include "GLSL.h"
#include "Widgets.h"
#include "Camera.h"
#include "vec.h"
#include "CheckError.h"
#include <vector>
#include "Mesh.h"
#include <cstdlib>


//****** Globals

// IDs for vertex buffer, shader programs, uniforms
GLuint shader  		= -1;
GLuint vBufferID	= -1;
GLuint viewID		= -1;
GLuint vPosition	= -1;
GLuint vColor		= -1;
GLuint intensityID  = -1;

int winSize				= 700;
int winWidth		    = 700;
int winHeight			= 700;
float origin[]			= {0, 0, 0};

std::vector<vec3> vertices;

std::vector<int3> triangles;
std::vector<vec3> triangleCenters;
std::vector<vec3> triangleNormals;
std::vector<vec3> colors;

// tree containers
std::vector<vec3> attractionPoints;
std::vector<vec3> treeNodes;
std::vector<int2> lines;
int branchCount = 0;

// tree number of attraction points
const int numAttract = 10000;

const float treeDrawDistance = .1f;
const float influenceRad = .35f;
const float killDistance = .2f;
const int timeoutCount = 4000;

// tree center
float center[]			= {.4f, .4f, .2f};

// tree control sphere
const float tree_radius = .9f;

// tree control base
float sphereBase[] = {0,.9f, 0};

// shader uniforms
mat4 view;

// colors
float blk[] = {0, 0, 0}, blu[] =  {0, 0, 1};

// controls
Slider fovSlider(20, 20, 150, 0, 45, 25);

// camera
Camera camera(winWidth, winHeight, 25, 3);

// light
vec3 sun(.9f, .3f, .9f);

//****** Shaders

 char *vShader = "\
	#version 130\n\
    uniform mat4 view;\n\
	uniform float intensity;\n\
	in vec3 position;\n\
	in vec3 color;\n\
	out vec4 vColor;\n\
	void main()\n\
	{\n\
		vec4 xp = view*vec4(position, 1);\n\
		gl_Position = xp; // /xp.w;\n\
	    vColor = vec4(intensity*color, 1);\n\
	}\n";

 char *fShader = "\
	#version 130\n\
	in vec4 vColor;\n\
	out vec4 fColor;\n\
	void main()\n\
	{\n\
        //if (!gl_FrontFacing)\n\
        //    discard;\n\
        fColor = vColor;\n\
	}\n";

 void InitShader()
{
    // build shader programs from inline code
	shader = GLSL_LinkProgramViaCode(vShader, fShader);
    
    // status report
    if (shader)
        GLSL_PrintProgramAttributes(shader);
    else
        printf("Failed to link shader program\n");

    // set vertex position location and enable 
    vPosition = glGetAttribLocation(shader, "position");
    glEnableVertexAttribArray(vPosition);

    // set vertex color location and enable
    vColor = glGetAttribLocation(shader, "color");
    glEnableVertexAttribArray(vColor);

    // set uniform IDs
    viewID = glGetUniformLocation(shader, "view");
	intensityID = glGetUniformLocation(shader, "intensity");

    GLSL_PrintProgramUniforms(shader);
	glUseProgram(shader);
}

// start with one trunk of tree nodes
// These are hard-coded starting points
void InitTree()
{
	treeNodes.push_back(vec3(0,-.9f, 0));
	branchCount++;

	treeNodes.push_back(vec3(0,-.8f, 0));
	lines.push_back(int2(branchCount - 1, branchCount));
	branchCount++;

	treeNodes.push_back(vec3(0,-.7f, 0));
	lines.push_back(int2(branchCount - 1, branchCount));
	branchCount++;

	treeNodes.push_back(vec3(0,-.6f, 0));
	lines.push_back(int2(branchCount - 1, branchCount));
	branchCount++;

	treeNodes.push_back(vec3(0,-.4f, 0));
	lines.push_back(int2(branchCount - 1, branchCount));
	branchCount++;

	treeNodes.push_back(vec3(0,-.2f, 0));
	lines.push_back(int2(branchCount - 1, branchCount));
	branchCount++;

	treeNodes.push_back(vec3(0,-.15f, 0));
	lines.push_back(int2(branchCount - 1, branchCount));
	branchCount++;

	treeNodes.push_back(vec3(0,-.1f, 0));
	lines.push_back(int2(branchCount - 1, branchCount));
	branchCount++;

	treeNodes.push_back(vec3(0,-.05f,0));
	lines.push_back(int2(branchCount - 1, branchCount));
	branchCount++;

	treeNodes.push_back(vec3(0,0,0));
	lines.push_back(int2(branchCount - 1, branchCount));
	branchCount++;

	treeNodes.push_back(vec3(0,.05f,0));
	lines.push_back(int2(branchCount - 1, branchCount));
	branchCount++;

	treeNodes.push_back(vec3(0,.1f,0));
	lines.push_back(int2(branchCount - 1, branchCount));
	branchCount++;
}

// put the generated attraction points into a linked list
void GenAttractionPoints(int n)
{
	for (int i = 0; i < n; i++)
	{
		// generate attraction points by spherical coordinates
		// select two angles theta and phi from 0 to 360 (with chances to hit half degrees
		float theta_degrees = (float)(std::rand() % 721)/2;
		float phi_degrees = (float)(std::rand() % 721)/2;

		// convert to radians
		float theta_angle = 3.141592f*theta_degrees/180.f;
		float phi_angle = 3.141592f*phi_degrees/180.f;

		int random = std::rand();

		// select a random distance to the control sphere radius
		float p = (float)(random % 100 + 1)/100 * tree_radius;

		// establish random points by spherical coordinates
		vec3 vtemp;
		vtemp.x = p * sin(phi_angle) * cos(theta_angle);
		vtemp.y = p * sin(phi_angle) * sin(theta_angle);
		vtemp.z = p * cos(phi_angle);

		// shift by the center of the control sphere
		vtemp.x += sphereBase[0];
		vtemp.y += sphereBase[1];
		vtemp.z += sphereBase[2];

		// store the random point in an array

		attractionPoints.push_back(vtemp);
	}
}

float Magnitude(vec3 v)
{
	return sqrt(pow(v.x, 2) +
		pow(v.y, 2) +
		pow(v.z, 2));
}

float Distance(vec3 v1, vec3 v2)
{
	float distance = sqrt(
				pow((v1.x - v2.x), 2) + 
				pow((v1.y - v2.y), 2) + 
				pow((v1.z - v2.z), 2));
	return distance;
}

//****** Tree Skeleton
void InitTreeSkeleton()
{
	if (attractionPoints.empty() ||
		branchCount > timeoutCount ||
		lines.size() > 50000) // hardcoded limiters to make sure that the tree skeleton recursion will stop eventually
		return;

	// The tree will be defined using the space colonization algorithm.

	std::vector<std::vector<vec3>> influencePoints; // parallel vector to tree nodes to contain closest influence attraction points
	influencePoints.resize(treeNodes.size());

	for(int i = 0; i < attractionPoints.size(); i++)
	{
		float minDistance = tree_radius;
		float closestNode = -1;

		// Computer the distance of the attraction point to each tree node
		for (int j = 0; j < treeNodes.size(); j++)
		{
			float distance_to_node = Distance(attractionPoints[i], treeNodes[j]);

			// find the minimum distance to a tree node
			if (distance_to_node <= influenceRad && 
				distance_to_node < minDistance)
			{
				minDistance = distance_to_node;
				closestNode = j;
			}
		}

		if(closestNode > -1)
		// associate the attraction point to it's closest node
			influencePoints[closestNode].push_back(attractionPoints[i]);
	}

	int oldBranchCount = branchCount;

	// Compute the average direction for each of the attraction points influencing the tree nodes
	for (int i = 0; i < influencePoints.size(); i++)
	{
		vec3 totalDirection(0,0,0);
		vec3 avgDirection(0,0,0);

		if(influencePoints[i].size() > 0)
		{
			for (int j = 0; j < influencePoints[i].size(); j++)
				totalDirection = totalDirection + influencePoints[i][j] / Magnitude(influencePoints[i][j]);

			// calculate the average direction
			avgDirection = totalDirection / influencePoints[i].size();

			// Create a new tree node distance treeDrawDistance away from the node in the direction avgDirection
			vec3 vPrime = treeNodes[i] + treeDrawDistance * avgDirection;

			treeNodes.push_back(vPrime);
			int2 newLine = int2( i, branchCount++); 
			lines.push_back(newLine);
		}
	}

	// Delete attraction points
	for (int i = oldBranchCount; i < treeNodes.size(); i++)
	{
		// check the attraction points for kill distance
		for (int j = 0; j < attractionPoints.size(); j++)
		{
			if (Distance(attractionPoints[j], treeNodes[i]) <= killDistance)
				attractionPoints.erase(attractionPoints.begin() + j);
		}
	}

	InitTreeSkeleton();
}

//****** GPU transfer

void InitVertexBuffer()
{
    // create a "vertex array object" and make it the active vertex array
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // create a vertex buffer for the array, and make it the active vertex buffer
    glGenBuffers(1, &vBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vBufferID);

	InitTree();
	GenAttractionPoints(numAttract);
	InitTreeSkeleton();

	vertices = treeNodes;

	colors.resize(vertices.size());
	for (int i = 0; i < (int) vertices.size(); i++)
		for (int k = 0; k < 3; k++)
			colors[i][k] = 0;

	// initialize the camera view
	view = camera.GetView();

    // allocate buffer memory to hold vertex locations and colors
	glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(vec3)+colors.size()*sizeof(vec3), NULL, GL_STATIC_DRAW);

    // load location and color data to the buffer
    glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size()*sizeof(vec3), &vertices[0]);
    glBufferSubData(GL_ARRAY_BUFFER, vertices.size()*sizeof(vec3), colors.size()*sizeof(vec3), &colors[0]);
}


//****** GLUT callbacks

void MouseButton(int bun, int state, int x, int y)
{
	// wish upward-facing y
	y = winSize-y;

	// mouse up
	if(state == GLUT_UP)
		camera.MouseUp();

	// mouse down
	else{
		int mods  = glutGetModifiers();
		bool shft = (mods & GLUT_ACTIVE_SHIFT) != 0;
		bool ctrl = (mods & GLUT_ACTIVE_CTRL) != 0;
		bool alt  = (mods & GLUT_ACTIVE_ALT) != 0;
		camera.MouseDown(x, y, shft, ctrl, alt);
	}

	glutPostRedisplay();
}

void MouseDrag(int x, int y)
{
    // user has moved the mouse while mouse-down
	//fovSlider.Mouse(x, winSize-y);
	////SetView();
 //   glutPostRedisplay();
	camera.MouseDrag(x, winSize-y);
	view = camera.GetView();
	glutPostRedisplay();
}

void Close()
{
	// unbind vertex buffer, free GPU memory
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &vBufferID);
}


//****** Display

 void DisplayMatrix(mat4 m, float x, float y)
 {
	 // x, y in [-1, 1]
	char buf[100];
	for (int row = 0; row < 4; row++)
		for (int col = 0; col < 4; col++) {
			sprintf(buf, "%3.1f", m[col][row]);			// transpose
			glRasterPos2f(x+col*.15f, y-row*.15f); // supported in OpenGL 2.1, but deperecated 3.1
			glutBitmapString(GLUT_BITMAP_HELVETICA_18, (unsigned char*) buf);
		}
 }

void DrawLines()
{
	// connnect vBuffer to vertex shader inputs
	glBindBuffer(GL_ARRAY_BUFFER, vBufferID);
	glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);
	glVertexAttribPointer(vColor, 3, GL_FLOAT, GL_FALSE, 0, (void*)(vertices.size()*sizeof(vec3)));

	int nLines = (int)lines.size();

	for (int i = 0; i < nLines; i++)
	{
		glDrawElements(GL_LINES, 2, GL_UNSIGNED_INT, &lines[i]);
	}
}
/*void DrawTriangles()
{
	// connect vBuffer to vertex shader inputs
	glBindBuffer(GL_ARRAY_BUFFER, vBufferID);
	glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);
	glVertexAttribPointer(vColor, 3, GL_FLOAT, GL_FALSE, 0, (void *)(vertices.size()*sizeof(vec3)));

	int nTriangles = (int)triangles.size();

	for(int i = 0; i < nTriangles; i++)
	{
		vec3 &center = triangleCenters[i];
		vec3 &normal = triangleNormals[i];

		vec3 light = normalize(sun-center);

		float intensity = dot(light, normal);

		if(intensity < 0)
			intensity = 0;

		glUniform1f(intensityID, intensity);

		glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, &triangles[i]);
	}

	// draw triangles
	//glDrawElements(GL_TRIANGLES, sizeof(triangles)/sizeof(int), GL_UNSIGNED_INT, &triangles[0]);
	//glDrawElements(GL_TRIANGLES, triangles.size()*3, GL_UNSIGNED_INT, &triangles[0]);
}*/

void Display()
{

	// initialize the camera view
	view = camera.GetView();

    glClearColor(.5, .5, .5, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set smooth
	glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_POINT_SPRITE);		// see Dots.doc
	glEnable(GL_POINT_SMOOTH);	// **** fails! (as does GLUT_MULTISAMPLE, which is slow)
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

	// download view matrix to GPU and display
    glUniformMatrix4fv(viewID, 1, true, (float *) view);

	// draw lines
	DrawLines();

	// show view matrix
	DisplayMatrix(view, -.9f, .9f);

	// draw tetrahedron center
	glLineWidth(.6f);

	// draw widgets in pixel space
	mat4 screen = ScreenMode(winSize, winSize);
	glUniformMatrix4fv(viewID, 1, true, (float *) screen);
	glDisable(GL_BLEND);
	fovSlider.Draw("fov");

	// enable depth buffer
	glEnable(GL_DEPTH_TEST);
	glClear(GL_DEPTH_BUFFER_BIT);

	// finish
	CheckGL_Errors("");
    glFlush();
}


//****** Application

void main(int ac, char **av)
{

	// init GLUT and GLEW
    glutInit(&ac, av);
    glutInitWindowSize(winSize, winSize);
    glutCreateWindow("Rotation (3D) Example");
    glewInit();

	// init shader and GPU data
    InitVertexBuffer();
    InitShader();

	// set GLUT callbacks and begin
    
    //glutKeyboardFunc(Keyboard);
    
    //glutIdleFunc(Idle);
	glutCloseFunc(Close);
	glutDisplayFunc(Display);
    glutMotionFunc(MouseDrag);
	glutMouseFunc(MouseButton);
	glutMotionFunc(MouseDrag);
    glutMainLoop();
}
