/*  =====================================
    Widget.h - Butns, sliders, etc
    Copyright (c) Jules Bloomenthal, 2012
    All rights reserved
    ===================================== */

#ifndef WIDGET_HDR
#define WIDGET_HDR

#include <string>
#include "mat.h"
//using namespace std;

#pragma warning(disable:4786) // kill MS warning re long names, nested templates


//****** Buttons

class Button
{
public:
    int x, y, w, h, winW, winH; // in pixels
    Button(int x, int y, int w, int h);
    void Draw(char *name, float *color);
    bool Hit(int x, int y);
};


//****** Sliders

class Slider
{
public:
	int winW, winH;	// window size
    int x, y;				// lower-left location, in pixels
    int size;				// length, in pixels
    int loc;				// slider x-position, in pixels
    float min, max;
    Slider(int x, int y, int size, float min, float max, float init);
    void Draw(char *name);
    float GetValue();
    bool Mouse(int x, int y);
		// called upon mouse-down or mouse-drag
};


//******Movers

class Mover {
public:
    float p1[3], p2[3]; // DEBUG (normally local to SetPlane)

    float point[3];
    float plane[4];
    void SetPlane(int x, int y, mat4 &modelview, mat4 &persp);
    void Pick(const float *p, int x, int y, mat4 &modelview, mat4 &persp);
    void Drag(int x, int y, mat4 &modelview, mat4 &persp);
};

#endif
