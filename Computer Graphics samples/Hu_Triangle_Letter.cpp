/*  =======================================================
    Author: Eric Hu
	Hu_Triangle_Letter.cpp
	(modified DrawRetainedBuffer.cpp
    Copyright (c) Jules Bloomenthal, 2012
    All rights reserved)
    =======================================================
    Use OpenGL to draw a triangle in two retained vertex array mode
	Code from "Modern OpenGL Usage: Using Vertex Buffer Objects Well" by Mark Kilgard
*/

#include <vector>
#include "glew.h"
#include "glut.h"


//****** Globals

// application window size in pixels
int      winSize = 400;

GLsizei  nBytesPerVertex = sizeof(GLfloat)*5;                // "stride"
GLsizei  nBytesPerArray = 12*nBytesPerVertex;

// 12 vertices
float places[][2] = {{50, 50},{50, 350},{250, 350},{250, 300},{100, 300},{100, 250},
					{200, 250},{200, 200},{100, 200},{100, 100},{250, 100},{250, 50}}; // X,Y in pixels

float colors[][3]    = {{ 1, 1, 1}, { 1, 0, 0}, {.5, 0, 0}, {1, 1, 0},  {.5, 1, 0},
{ 0, 1, 0}, { 0, 1, 1}, {0,  0, 1}, {1, 0, 1},  {.5, 0, .5}, {.5, .5, .5}, {0, 0, .5}}; // R, G, B in range [0,1]

// 9 triangles
int triangles[][3] = {{0, 1, 8}, {1, 2, 4}, {2, 3, 4}, {1, 4, 8}, {5, 6, 8},
					{6, 7, 8}, {8, 9, 0}, {9, 10, 11}, {9, 11, 0}};

// vertex buffer
//     from white paper by Kilgard:
//     vertex buffers provide storage for vertex arrays in "specially negotiated
//     regions" of memory maintained as OpenGL buffer objects that both the
//     application and the OpenGL implementation can access and the OpenGL API
//     can arbitrate a policy for consistent access to this memory - OpenGL can
//     allow the GPU direct memory access; the CPU is not involved in data
//     transfer if the vertex data is stored in a buffer object
GLuint   vBufferID = -1;


//****** Initializations 

float *InitVertexArray()
{
	// allocate CPU memory for three triangle vertices
    float *p = new float[nBytesPerArray];
	// fill p in same manner as DrawTriangleImmediateMode(below) passes data to OpenGL
	for (int i = 0; i < 12; i++) {
	    // for each vertex, write RGB color, then XY location
		memcpy(&p[0+5*i], &colors[i], 3*sizeof(GLfloat));
		memcpy(&p[3+5*i], &places[i], 2*sizeof(GLfloat));
	}
	// return address of CPU memory
	return p;
}

void InitVertexBuffer()
{
	// create vertex array
	float *p = InitVertexArray();
	// ask GL for unique identifier for buffer (presumably on GPU)
	glGenBuffers(1, &vBufferID);
	// change GL state so subsequent array operations concern this buffer
	glBindBuffer(GL_ARRAY_BUFFER, vBufferID);
	// copy vertex array data to GPU
	glBufferData(GL_ARRAY_BUFFER, nBytesPerArray, p, GL_STATIC_DRAW);
	// free vertex array, which is no longer needed
	delete [] p;
}


//****** Callbacks

void Draw()
{
	// called by GLUT whenever window is to be displayed`	
    glClear(GL_COLOR_BUFFER_BIT);
	glBindBuffer(GL_ARRAY_BUFFER, vBufferID);
	const char *base = NULL; // causes GPU to 'pull' data via direct memory access
	glColorPointer(/*rgb*/3, GL_FLOAT, nBytesPerVertex, base+0*sizeof(GLfloat));
	glVertexPointer(/*xy*/2, GL_FLOAT, nBytesPerVertex, base+3*sizeof(GLfloat));
	glDrawElements(GL_TRIANGLES, 27, GL_UNSIGNED_INT, triangles);
    glFlush();
}

void Reshape(int w, int h)
{
	// called by GLUT whenever application is resized
	glViewport(0, 0, w, h);       // establish viewing area to cover entire window
	glMatrixMode(GL_PROJECTION);  // modify projection matrix
	glLoadIdentity();             // reset projection matrix
	glOrtho(0, w, 0, h, -1, 1);   // map coordinate system directly to window coords
	glScalef(1, -1, 1);           // invert Y axis so increasing Y downwards
	glTranslatef(0, -h, 0);       // shift origin to upper-left corner
}


//****** Application

void main(int ac, char **av)
{
    // init window
    Reshape(winSize, winSize);
    glutInitWindowSize(winSize, winSize);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(av[0]);
	glClearColor(.65, .5, .5, 1);

    // GLUT callbacks
    glutDisplayFunc(Draw);
    glutReshapeFunc(Reshape);

	// glGenBuffers is OpenGL v.1.5; obtain OpenGL extension bindings with glew
	glewInit();

	// initialize array or buffer
	InitVertexBuffer();

	// enable those arrays OpenGL to access during DrawArrays call
	glEnableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);

    // begin event handler
    glutMainLoop();
}

