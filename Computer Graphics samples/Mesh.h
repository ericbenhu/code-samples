/*	==============================
    Mesh.h - 3D mesh of triangles
    Copyright (c) Jules Bloomenthal, 2012
    All rights reserved
	=============================== */

#ifndef MESH_HDR
#define MESH_HDR

#include <vector>
#include "mat.h"

struct int2 {
	int i1, i2;
	int2() {}
	int2(int i1, int i2) :
		i1(i1), i2(i2) {}
};

struct int3 {
	int i1, i2, i3;
	int3() {}
	int3(int i1, int i2, int i3) :
		i1(i1), i2(i2), i3(i3) {}
};

bool ReadAsciiObj(char *filename,
								  std::vector<vec3>	&vertices,
								  std::vector<int3>	&triangles,
								  std::vector<vec3>	*vertexNormals = NULL,
								  std::vector<vec2>	*vertexTextures = NULL,
								  std::vector<int>		*triangleGroups = NULL);

void Normalize(std::vector<vec3> &vertices);
	// translate and apply uniform scale so that vertices all fit within +/- 1

#endif
