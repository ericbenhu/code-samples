/*  =======================================================
    TetRotate.cpp
    Draw rotating tetrahedron with perspective
    Copyright (c) Jules Bloomenthal, 2012
    All rights reserved
    =======================================================
*/

#include <time.h>
#include <gl/glew.h>
#include <gl/freeglut.h>
#include "mat.h"
#include "Draw.h"
#include "GLSL.h"
#include "Widgets.h"
#include "Camera.h"
#include "vec.h"
#include "CheckError.h"
#include <vector>
#include "Mesh.h"


//****** Globals

// IDs for vertex buffer, shader programs, uniforms
GLuint shader  		= -1;
GLuint vBufferID	= -1;
GLuint viewID		= -1;
GLuint vPosition	= -1;
GLuint vColor		= -1;
GLuint intensityID  = -1;

int winSize				= 700;
int winWidth		    = 700;
int winHeight			= 700;
float origin[]			= {0, 0, 0};

std::vector<vec3> vertices;
std::vector<int3> triangles;
std::vector<vec3> triangleCenters;
std::vector<vec3> triangleNormals;
std::vector<vec3> colors;
//// 5 vertices
//float a						= 1.f/sqrt(3.f);
//float b						= 2.f*sqrt(2.f)*a;
//float points[][3]		= {{-1.f, -a, 0}, {0, 2.f*a, 0}, {1.f, -a, 0}, {0, 0, b}, {0, 0, -b}};
//float colors[][3]		= {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {1, 1, 1}, {0, 0, 0}};
//
//// 6 faces, 9 edges (two tetrahedra joined at base)
//int triangles[][3]	= {{0, 3, 1}, {1, 3, 2}, {2, 3, 0}, {1, 4, 0}, {2, 4, 1}, {0, 4, 2}};
//int segments[][2]	= {{0, 1}, {1, 2}, {2, 0}, {0, 3}, {1, 3}, {2, 3}, {0, 4}, {1, 4}, {2, 4}};

// tetrahedra center
float center[]			= {.4f, .4f, .2f};
float normal[];

// shader uniforms
mat4 view;

// colors
float blk[] = {0, 0, 0}, blu[] =  {0, 0, 1};

// controls
Slider fovSlider(20, 20, 150, 0, 45, 25);

// animation
clock_t prevTime	= clock();
float angle[]			= {0, 0, 0};			// degrees
float dAngle[]		= {35, 50, 70};	// deg/sec
bool paused			= false;

// camera
Camera camera(winWidth, winHeight, 25, 3);

// light
vec3 sun(.9f, .3f, .9f);

//****** Shaders

 char *vShader = "\
	#version 130\n\
    uniform mat4 view;\n\
	uniform float intensity;\n\
	in vec3 position;\n\
	in vec3 color;\n\
	out vec4 vColor;\n\
	void main()\n\
	{\n\
		vec4 xp = view*vec4(position, 1);\n\
		gl_Position = xp; // /xp.w;\n\
	    vColor = vec4(intensity*color, 1);\n\
	}\n";

 char *fShader = "\
	#version 130\n\
	in vec4 vColor;\n\
	out vec4 fColor;\n\
	void main()\n\
	{\n\
        //if (!gl_FrontFacing)\n\
        //    discard;\n\
        fColor = vColor;\n\
	}\n";

 void InitShader()
{
    // build shader programs from inline code
	shader = GLSL_LinkProgramViaCode(vShader, fShader);
    
    // status report
    if (shader)
        GLSL_PrintProgramAttributes(shader);
    else
        printf("Failed to link shader program\n");

    // set vertex position location and enable 
    vPosition = glGetAttribLocation(shader, "position");
    glEnableVertexAttribArray(vPosition);

    // set vertex color location and enable
    vColor = glGetAttribLocation(shader, "color");
    glEnableVertexAttribArray(vColor);

    // set uniform IDs
    viewID = glGetUniformLocation(shader, "view");
	intensityID = glGetUniformLocation(shader, "intensity");

    GLSL_PrintProgramUniforms(shader);
	glUseProgram(shader);
}


//****** Matrix Operations

//void Identity(float m[][4])
//{
//    for (int i = 0; i < 4; i++)
//        for (int j = 0; j < 4; j++)
//            m[i][j] = i == j? 1.f : 0.f;
//}
//
//void Multiply(float a[][4], float b[][4], float result[][4])
//{
//	for (int i = 0; i < 4; i++)
//		for (int j = 0; j < 4; j++) {
//			float d = 0;
//			for (int k = 0; k < 4; k++)
//				d += a[i][k]*b[k][j];
//			result[i][j] = d;
//		}
//}
//
//void Concatenate(float a[][4], float b[][4])
//{
//    float temp[4][4];
//    Multiply(a, b, temp);
//    memcpy(a, temp, sizeof(temp));
//}
//
//void Rotate(float m[][4], float deg, int axis)
//{
//    float rad = 3.141592f*deg/180.f;
//    float c = cos(rad), s = sin(rad);
//    int i0 = (axis+2)%3, i1 = (axis+1)%3;
//    float temp[4][4];
//    Identity(temp);
//    temp[i0][i0] = temp[i1][i1] = c;
//    temp[i1][i0] = -(temp[i0][i1] = s);
//    Concatenate(m, temp);
//}
//
//void Scale(float m[][4], float s)
//{
//	float temp[4][4];
//	Identity(temp);
//	temp[0][0] = temp[1][1] = temp[2][2] = s;
//	Concatenate(m, temp);
//}
//
//void Translate(float m[][4], float t[3])
//{
//    float temp[4][4];
//    Identity(temp);
//    for (int i = 0; i < 3; i++)
//        temp[3][i] = t[i];
//    Concatenate(m, temp);
//}
//
//void Perspective(float m[][4],
//								const GLfloat fovy, const GLfloat aspect,
//								const GLfloat zNear, const GLfloat zFar)
//{
//	float fovyRadians = 3.1415f*fovy/180.f;
//    GLfloat top = tan(fovyRadians/2)*zNear;
//    GLfloat right = top*aspect;
//	float temp[4][4];
//	Identity(temp);
//    temp[0][0] = zNear/right;
//    temp[1][1] = zNear/top;
//    temp[2][2] = -(zFar+zNear)/(zFar-zNear);
//    temp[3][2] = -2*zFar*zNear/(zFar-zNear);
//    temp[2][3] = -1;
//    Concatenate(m, temp);
//}


//****** Animation

// set view transformation matrix according to above matrix routines
//void SetView()
//{
//	float m[4][4];
//	Identity(m);
//	// the following matrix routines concatenate results into m
//	Rotate(m, angle[0], 0); // x-rotate
//	Rotate(m, angle[1], 1); // y
//	Rotate(m, angle[2], 2); // z
//	Scale(m, .2f);
//	Translate(m, center);
//	float dolly[] = {0, 0, -5};
//	Translate(m, dolly);
//	Perspective(m, fovSlider.GetValue(), 1, .01f, 5);
//	// transpose m to global view matrix
//	for (int i = 0; i < 4; i++)
//		for (int j = 0; j < 4; j++)
//			view[i][j] = m[j][i];
//}

// set view transformation matrix using mat.h
//void SetView()
//{
//	// model_view matrices
//	mat4 offset	= Translate(center[0], center[1], center[2]);
//	mat4 scale	= Scale(.2f, .2f, .2f);
//	mat4 rotx		= RotateX(angle[0]);
//	mat4 roty		= RotateY(angle[1]);
//	mat4 rotz		= RotateZ(angle[2]);
//
//	// camera matrices		
//	float aspect	= 1;
//	mat4 dolly		= Translate(0, 0, -5);
//	mat4 persp	= Perspective(fovSlider.GetValue(), aspect, .01f, 5);
//
//	// set view matrix
//	view = (persp*dolly)*(offset*rotx*roty*rotz*scale);
//}


//****** GPU transfer

void InitVertexBuffer()
{
    // create a "vertex array object" and make it the active vertex array
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // create a vertex buffer for the array, and make it the active vertex buffer
    glGenBuffers(1, &vBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vBufferID);

	if (!ReadAsciiObj("cow.obj", vertices, triangles)) {
		printf("can't read file\n");
		getchar();
		return;
	}

	Normalize(vertices);

	colors.resize(vertices.size());
	for (int i = 0; i < (int) vertices.size(); i++)
		for (int k = 0; k < 3; k++)
			colors[i][k] = 1;

	int nTriangles = (int) triangles.size();
	triangleNormals.resize(nTriangles);
	triangleCenters.resize(nTriangles);

	for (int i = 0; i < nTriangles; i++)
	{
		int3 t = triangles[i];
		vec3 &center = triangleCenters[i];
		vec3 &normal = triangleNormals[i];
		
		// three triangle vertices
		vec3 &a = vertices[t.i1], &b = vertices[t.i2], &c = vertices[t.i3];
		// two vectors in the plane of the triangle
		vec3 v1(b-a), v2(c-b);
		// set center and normal
		center = (a+b+c)/3;
		normal = normalize(cross(v1, v2));
	}

	// initialize the camera view
	view = camera.GetView();

    // allocate buffer memory to hold vertex locations and colors
	glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(vec3)+colors.size()*sizeof(vec3), NULL, GL_STATIC_DRAW);

    // load location and color data to the buffer
    glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size()*sizeof(vec3), &vertices[0]);
    glBufferSubData(GL_ARRAY_BUFFER, vertices.size()*sizeof(vec3), colors.size()*sizeof(vec3), &colors[0]);
}


//****** GLUT callbacks

//void Idle()
//{
//	// compute elapsed time
//    clock_t now = clock();
//    float dt = (float) (now-prevTime)/CLOCKS_PER_SEC;
//    prevTime = now;
//
//	// update rotation angles
//   for (int i = 0; i < 3; i++)
//        angle[i] += dt*dAngle[i];
//
//   //SetView();
//   glutPostRedisplay();
//}

void MouseButton(int bun, int state, int x, int y)
{
	// wish upward-facing y
	y = winSize-y;

	// mouse up
	if(state == GLUT_UP)
		camera.MouseUp();

	// mouse down
	else{
		int mods  = glutGetModifiers();
		bool shft = (mods & GLUT_ACTIVE_SHIFT) != 0;
		bool ctrl = (mods & GLUT_ACTIVE_CTRL) != 0;
		bool alt  = (mods & GLUT_ACTIVE_ALT) != 0;
		camera.MouseDown(x, y, shft, ctrl, alt);
	}

	glutPostRedisplay();
}

//void Keyboard(unsigned char key, int x, int y)
//{
//	if (key == 27) // escape
//        glutLeaveMainLoop();
//    if (key == 'p') {
//        paused = !paused;
//        prevTime = clock();
//        glutIdleFunc(paused? NULL : Idle);
//    }
//    if (key == 'r')
//        for (int i = 0; i < 3; i++)
//            dAngle[i] = -dAngle[i];
//}

void MouseDrag(int x, int y)
{
    // user has moved the mouse while mouse-down
	//fovSlider.Mouse(x, winSize-y);
	////SetView();
 //   glutPostRedisplay();
	camera.MouseDrag(x, winSize-y);
	view = camera.GetView();
	glutPostRedisplay();
}

void Close()
{
	// unbind vertex buffer, free GPU memory
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &vBufferID);
}


//****** Display

 void DisplayMatrix(mat4 m, float x, float y)
 {
	 // x, y in [-1, 1]
	char buf[100];
	for (int row = 0; row < 4; row++)
		for (int col = 0; col < 4; col++) {
			sprintf(buf, "%3.1f", m[col][row]);			// transpose
			glRasterPos2f(x+col*.15f, y-row*.15f); // supported in OpenGL 2.1, but deperecated 3.1
			glutBitmapString(GLUT_BITMAP_HELVETICA_18, (unsigned char*) buf);
		}
 }

void DrawTriangles()
{
	// connect vBuffer to vertex shader inputs
	glBindBuffer(GL_ARRAY_BUFFER, vBufferID);
	glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);
	glVertexAttribPointer(vColor, 3, GL_FLOAT, GL_FALSE, 0, (void *)(vertices.size()*sizeof(vec3)));

	int nTriangles = (int)triangles.size();

	for(int i = 0; i < nTriangles; i++)
	{
		vec3 &center = triangleCenters[i];
		vec3 &normal = triangleNormals[i];

		vec3 light = normalize(sun-center);

		float intensity = dot(light, normal);

		if(intensity < 0)
			intensity = 0;

		glUniform1f(intensityID, intensity);

		glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, &triangles[i]);
	}

	// draw triangles
	//glDrawElements(GL_TRIANGLES, sizeof(triangles)/sizeof(int), GL_UNSIGNED_INT, &triangles[0]);
	//glDrawElements(GL_TRIANGLES, triangles.size()*3, GL_UNSIGNED_INT, &triangles[0]);
}

void Display()
{

	// initialize the camera view
	view = camera.GetView();

    glClearColor(.5, .5, .5, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set smooth
	glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_POINT_SPRITE);		// see Dots.doc
	glEnable(GL_POINT_SMOOTH);	// **** fails! (as does GLUT_MULTISAMPLE, which is slow)
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

	// download view matrix to GPU and display
    glUniformMatrix4fv(viewID, 1, true, (float *) view);

	// draw faces
	DrawTriangles();

	// draw edges
	/*glLineWidth(1.4f);
	int nsegs = sizeof(segments)/(2*sizeof(int));
	for (int i = 0; i < nsegs;  i++) {
		int *s = segments[i];
		Line(points[s[0]], points[s[1]], blk);
	}*/

	// show view matrix
	DisplayMatrix(view, -.9f, .9f);

	// draw tetrahedron center
	glLineWidth(.6f);
	Asterisk(origin, .1f, blu);

	// draw widgets in pixel space
	mat4 screen = ScreenMode(winSize, winSize);
	glUniformMatrix4fv(viewID, 1, true, (float *) screen);
	glDisable(GL_BLEND);
	fovSlider.Draw("fov");

	// enable depth buffer
	glEnable(GL_DEPTH_TEST);
	glClear(GL_DEPTH_BUFFER_BIT);

	// finish
	CheckGL_Errors("");
    glFlush();
}


//****** Application

void main(int ac, char **av)
{

	// init GLUT and GLEW
    glutInit(&ac, av);
    glutInitWindowSize(winSize, winSize);
    glutCreateWindow("Rotation (3D) Example");
    glewInit();

	// init shader and GPU data
    InitVertexBuffer();
    InitShader();

	// set GLUT callbacks and begin
    glutDisplayFunc(Display);
    //glutKeyboardFunc(Keyboard);
    glutCloseFunc(Close);
    //glutIdleFunc(Idle);
    glutMotionFunc(MouseDrag);
	glutMouseFunc(MouseButton);
	glutMotionFunc(MouseDrag);
    glutMainLoop();
}
