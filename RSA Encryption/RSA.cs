﻿// Eric Hu
// October 27, 2012
// Computer Security - Dr. Roshandel
// RSA.cs

/* Description:
 * RSA.cs is a class that is an engine for generating keys as well as encrypting and decrypting
 * messages using the generated keys.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

class RSA
{
    // public key
    private BigInteger pubKey;
    // private key
    private BigInteger privKey;

    // default certainty for number of loops to do Miller-Rabin
    private const int defaultCertainty = 20;

    // Constructors:
    // Default Constructor
    public RSA()
    {
    }

    // Description: AKS primality test for a big integer
    public bool IsProbablePrime(BigInteger source, int certainty = defaultCertainty)
    {
        // The following code is based off of of a page from rosetta code
        // http://rosettacode.org/wiki/Miller-Rabin_primality_test#C.23

        if (source == 2 || source == 3)
            return true;
        if (source < 2 || source % 2 == 0)
            return false;

        BigInteger d = source - 1;
        int s = 0;

        while (d % 2 == 0)
        {
            d /= 2;
            s += 1;
        }

        Random rand = new Random();
        byte[] bytes = new byte[source.ToByteArray().LongLength];
        BigInteger a;

        for (int i = 0; i < certainty; i++)
        {
            do
            {
                rand.NextBytes(bytes);
                a = new BigInteger(bytes);
            }
            while (a < 2 || a >= source - 2);

            BigInteger x = BigInteger.ModPow(a, d, source);
            if (x == 1 || x == source - 1)
                continue;
            for (int r = 1; r < s; r++)
            {
                x = BigInteger.ModPow(x, 2, source);
                if (x == 1)
                    return false;
                if (x == source - 1)
                    break;
            }
            if (x != source - 1)
                return false;
        }
        return true;
    }

    public BigInteger GeneratePrime(BigInteger n)
    {
        while (!IsProbablePrime(n))
        {
            if(n.IsEven)
                n++;
            else 
                n = n + 2;
        }
        return n;
    }

    public void KeyGen(BigInteger p, BigInteger q)
    {
        // Make sure that p and q are prime
        if (!IsProbablePrime(p))
            throw new FormatException();
        if (!IsProbablePrime(q))
            throw new FormatException();

        // compute an n that is the product of two primes p and q
        BigInteger n = p * q;
        BigInteger totientN = (p - 1) * (q - 1);

        // choose an e that is relatively prime to totientN
        BigInteger e = 3;
        while (BigInteger.GreatestCommonDivisor(e, totientN) != 1 && e < totientN)
        {
            e = GeneratePrime(e + 1);
        }
        // solve for d by finding the multiplicative inverse of e
        BigInteger d = ModInverse(e, totientN);
        this.pubKey = e;
        this.privKey = d;
    }

    // Get for public and private keys
    public BigInteger PublicKey
    {
        get { return this.pubKey; }
    }
    public BigInteger PrivateKey
    {
        get { return this.privKey; }
    }

    // convert char to BigInteger
    public int ConvertChar(char c)
    {
        return (int)c;
    }
    // convert BigInteger to char
    public char ConvertInt(BigInteger b)
    {
        char c = Convert.ToChar((Int64)b);
        return c;
    }

    // encryption using RSA
    public string Encrypt(string plaintext, BigInteger pubKey, BigInteger n)
    {
        char[] cipher = new char[plaintext.Length];
        for (int i = 0; i < plaintext.Length; i++)
            cipher[i] = Encrypt(plaintext[i], pubKey, n);
        return new string(cipher);
    }
    public char Encrypt(char plainchar, BigInteger pubKey, BigInteger n)
    {
        BigInteger message = ConvertChar(plainchar);
        message = BigInteger.ModPow(message, pubKey, n);
        char cipherchar = ConvertInt(message);
        return cipherchar;
    }

    // decryption using RSA
    public string Decrypt(string ciphertext, BigInteger privKey, BigInteger n)
    {
        char[] message = new char[ciphertext.Length];
        for (int i = 0; i < ciphertext.Length; i++)
            message[i] = Decrypt(ciphertext[i], privKey, n);
        return new string(message);

    }
    public char Decrypt(char cipherchar, BigInteger privKey, BigInteger n)
    {
        BigInteger ciphertext = ConvertChar(cipherchar);
        ciphertext = BigInteger.ModPow(ciphertext, privKey, n);
        char message = ConvertInt(ciphertext);
        return message;
    }
    // Modular Inverse using the extended Euclidean Algorithm
    // Based off of code from http://pastebin.com/HpzUrGuj
    private BigInteger ModInverse(BigInteger a, BigInteger b)
    {
        BigInteger dividend = a % b;
        BigInteger divisor = b;

        BigInteger last_x = BigInteger.One;
        BigInteger curr_x = BigInteger.Zero;

        while (divisor.Sign > 0)
        {
            BigInteger quotient = dividend / divisor;
            BigInteger remainder = dividend % divisor;
            if (remainder.Sign <= 0)
                break;

            BigInteger next_x = last_x - curr_x * quotient;
            last_x = curr_x;
            curr_x = next_x;

            dividend = divisor;
            divisor = remainder;
        }

        if (divisor != BigInteger.One)
        {
            throw new Exception("Numbers a and b are not relatively primes");
        }
        return (curr_x.Sign < 0 ? curr_x + b : curr_x);
    }
}