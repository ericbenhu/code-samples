﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

class KeyPair
{
    // public key e and private key d
    private BigInteger pub;
    private BigInteger priv;

    public KeyPair(BigInteger e, BigInteger d)
    {
        this.pub = e;
        this.priv = d;
    }

    // Get functions for the key pair
    public BigInteger PrivateKey
    {
        get { return this.priv; }
    }
    public BigInteger PublicKey
    {
        get { return this.pub; }
    }
}
