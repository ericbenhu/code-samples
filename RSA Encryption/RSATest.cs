﻿// Eric Hu
// October 27, 2012
// Computer Security - Dr. Roshandel
// RSATest.cs

/* Description:
 * This is the main program for testing the RSA class
 * It should be able to test the functionality of the IsPrime() function and the GeneratePrime() function.
 * It will also test key generation, encryption, and decryption of files input by the user.
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.IO;

enum MenuOptions { ISPRIME = 1, GENPRIME, KEYGEN, ENCRYPT, DECRYPT, EXIT };

class Program
{
    static void Main(string[] args)
    {
        string []menu = {"Primality test", "Generate prime",
                                   "KeyGen", "Encrypt", "Decrypt", "Exit"};
        RSA rsa = new RSA();

        bool goflag = true;
        do
        {
            Console.WriteLine("Main Menu:");
            for (int i = 0; i < menu.Length; i++)
                Console.WriteLine("{0}- {1}", i + 1, menu[i]);
            Console.Write("-->");

            int response = int.Parse(Console.ReadLine());
            string inFile;
            string outFile;
            BigInteger input;
            BigInteger n;
            StreamReader sr;
            StreamWriter sw;

            switch (response)
            {
                    // Test Primality
                case(int)MenuOptions.ISPRIME:
                    Console.Write("Input integer:");
                    input = BigInteger.Parse(Console.ReadLine());
                    bool isprime = rsa.IsProbablePrime(input);
                    if (isprime)
                        Console.WriteLine("Is prime.");
                    else
                        Console.WriteLine("Is not prime.");
                    break;
                    // Test Prime generation
                case(int)MenuOptions.GENPRIME:
                    Console.Write("Input integer:");
                    input = BigInteger.Parse(Console.ReadLine());
                    BigInteger nextPrime = rsa.GeneratePrime(input);
                    Console.WriteLine("The next prime is {0}.", nextPrime);
                    break;
                    // Test Key generation
                case(int)MenuOptions.KEYGEN:
                    try
                    {
                        Console.Write("Enter first prime number:");
                        BigInteger prime1 = BigInteger.Parse(Console.ReadLine());
                        Console.Write("Enter second prime number:");
                        BigInteger prime2 = BigInteger.Parse(Console.ReadLine());
                        rsa.KeyGen(prime1, prime2);
                        Console.WriteLine("Public Key: {0}, Private Key: {1}", rsa.PublicKey, rsa.PrivateKey);
                    }
                    catch
                    {
                        Console.WriteLine("Error. Not a prime.");
                    }
                        break;
                    
                    // Test encryption
                case(int)MenuOptions.ENCRYPT:
                    Console.Write("Specify input filename:");
                    inFile = Console.ReadLine();
                    sr = new StreamReader(inFile);
                    
                    Console.Write("Specify output filename:");
                    outFile = Console.ReadLine();
                    sw = new StreamWriter(outFile);

                    Console.WriteLine("Specify encoding:");
                    Console.Write("Public key--> ");
                    BigInteger pubKey = BigInteger.Parse(Console.ReadLine());
                    Console.Write("N--> ");
                    n = BigInteger.Parse(Console.ReadLine());
                    string inPlaintext = sr.ReadToEnd();

                    string outCipertext = rsa.Encrypt(inPlaintext, pubKey, n);
                    sw.Write(outCipertext);

                    Console.WriteLine("{0} has been encrypted to {1}.", inFile, outFile);
                    sr.Close();
                    sw.Close();
                    break;
                    // Test decryption
                case(int)MenuOptions.DECRYPT:
                    Console.Write("Specify input filename:");
                    inFile = Console.ReadLine();
                    sr = new StreamReader(inFile);
                    
                    Console.Write("Specify output filename:");
                    outFile = Console.ReadLine();
                    sw = new StreamWriter(outFile);

                    Console.WriteLine("Specify encoding:");
                    Console.Write("Private key--> ");
                    BigInteger privKey = BigInteger.Parse(Console.ReadLine());
                    Console.Write("N--> ");
                    n = BigInteger.Parse(Console.ReadLine());
                    string inCiphertext = sr.ReadToEnd();

                    string outPlaintext = rsa.Decrypt(inCiphertext, privKey, n);
                    sw.Write(outPlaintext);
                    Console.WriteLine("{0} has been decrypted to {1}.", inFile, outFile);
                    sr.Close();
                    sw.Close();
                    break;
                case(int)MenuOptions.EXIT:
                    goflag = false;
                    break;
                default:
                    Console.WriteLine("Improper response");
                    break;
            }
            Console.WriteLine("******************************************");

        } while (goflag);
    }
}
