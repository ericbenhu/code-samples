// testing file for tree.c

#include <stdio.h>
#include "tree.h"

static void printTree(struct tree *root)
{
	if (root == NULL)
		return;
	
	// print the root then each of its children (pre-order)
	printf("%d\n", getData(root));
	printTree(getLeft(root));
	printTree(getRight(root));
}


int main(void)
{
	// create a tree with 5, 10, and 15
	struct tree *root = NULL;

	root = createTree(5, NULL, NULL);
	printf("%d\n", getData(root));
	
	setLeft(root, createTree(10, NULL,NULL));
	setRight(root, createTree(15, NULL, NULL));

	printTree(root);
	
	destroyTree(getRight(root));

	printTree(root);

	destroyTree(getLeft(root));
	
	printTree(root);
		
	destroyTree(root);
	
	printf("made it");
}
