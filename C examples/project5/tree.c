// Eric Hu

// Implementation for the tree

#include <stdio.h>
#include "tree.h"
#include <assert.h>

// define the tree structure
struct tree{
	int data;
	struct tree *left;
	struct tree *right;
	struct tree *parent;
};

// create a tree
// algorithmic complexity: O(1)
struct tree *createTree(int data, struct tree *left, struct tree *right)
{
	struct tree *root = malloc(sizeof(struct tree));// allocate memory
	assert(root!=NULL); /*check if the memory could be allocated */
	
	// initialize the data and the left and right pointers
	root->data = data;
	root->left = left;
	root->right = right;
	root->parent = NULL;

	// if left is not null, set left's parent to root
	if(left != NULL)
		left->parent = root;

	// if right is not null, set right's parent to root
	if(right != NULL)
		right->parent = root;

	// return the new tree
	return root;
}
// algorithmic complexity: O(n)
void destroyTree(struct tree *root)
{
	// use post-order traversal to delete each of the leaves before
	// deleting the root

	// base: if the subtree is NULL
	if (root == NULL)
		return;
	destroyTree(root->left);
	destroyTree(root->right);
	struct tree *temp = root;
	
	// check if the root has a parent first
	if (root->parent != NULL)
	{	
		// if the root is the left child, then set parent's left
		if (root == root->parent->left)
			root->parent->left = NULL;
		else // else, set the parent's right
			root->parent->right = NULL;
	}
	free(temp);

	return;
}

// return data from the root of the subtree
// algorithmic complexity: O(1)
int getData(struct tree *root)
{
	return root->data;
}

// return the left subtree
// algorithmic complexity: O(1)
struct tree *getLeft(struct tree *root)
{
	return root->left;
}

// return the right subtree
// algorithmic complexity: O(1)
struct tree *getRight(struct tree *root)
{
	return root->right;
}

// return the parent of the subtree
// algorithmic complexity: O(1)
struct tree *getParent(struct tree *root)
{
	return root->parent;
}

// update the left subtree of the root
// algorithmic complexity: O(1)
void setLeft(struct tree *root, struct tree *left)
{
	root->left = left;
	// when you set the left, you also set the parent of the left if 
	// it isn't NULL
	if (left != NULL)
		root->left->parent = root;
	
	return;
}

// update the right subtree of the root
// algorithmic complexity: O(1)
void setRight(struct tree *root, struct tree *right)
{
	root->right = right;
	// when you set the right, you also set the parent to the right if
	// it isn't NULL
	if (right != NULL)
		root->right->parent = root;

	return;
}
