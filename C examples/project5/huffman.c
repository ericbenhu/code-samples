// huffman coding application

// header files
#include<stdio.h>
#include<string.h>
#include"tree.h"
#include"pack.h"

// macros
#define p(x) (((x) - 1)/2)
#define lc(x) (2 *(x) + 1)
#define rc(x) (2 *(x) + 2)

// global variables
struct tree *nodes[257],*heap[257];// the heap is the priority queue
int hc = 0; /*heap count*/

// step 1: read from a file and count each of the characters
// step 2: create subtrees from each of the non-zero counts
// step 3: delete the two smallest subtrees from the heap and create a new
	// subtree with the deleted ones

// insert into a priority queue using a min heap
static void insert(struct tree* n)
{
	int x = hc;
	heap[hc++] = n;
	while(x > 0 && 
		getData(heap[x]) < 
		getData(heap[p(x)])){
			struct tree* temp = heap[x];
			heap[x] = heap[p(x)];
			heap[p(x)] = temp;
	}
}

// delete from a priority queue
static struct tree* delete()
{
	// look for the rightmost leaf on the lowest level which should be 
	// at the count index's location
	struct tree *temp;
	temp = heap[0];
	heap[0] = heap[--hc];
	heap[hc] = temp;
	
	int x = 0;

	// rearrange the tree by comparing the two children (if any)
	// if on child is non-existent, then heap checks the other and swaps
	// if necessary
	// if both children exist, then the two data values are compared
	// and a swap is made with the smaller one if necessary.
	while((lc(x) < hc) &&
		(getData(heap[lc(x)]) < getData(heap[x])) ||
		((rc(x) < hc) &&
		(getData(heap[rc(x)]) < getData(heap[x])))) { 
		if(rc(x) >= hc && lc(x) < hc && 
			getData(heap[lc(x)]) < getData(heap[x])){
			temp = heap[x];
			heap[x] = heap[lc(x)];
			heap[lc(x)] = temp;
			x = lc(x);
		}else if(lc(x) >= hc && rc(x) < hc &&
			getData(heap[rc(x)]) < getData(heap[x])){
			// swap right if data is less
			temp = heap[x];
			heap[x] = heap[rc(x)];
			heap[rc(x)] = temp;
			x = rc(x);
		}else if (lc(x) < hc && rc(x) < hc &&
			getData(heap[lc(x)]) < getData(heap[rc(x)])){
			temp = heap[x];
			heap[x] = heap[lc(x)];
			heap[lc(x)] = temp;
			x = lc(x);
		}else if(lc(x) < hc && rc(x) < hc){
			temp = heap[x];
			heap[x] = heap[rc(x)];
			heap[rc(x)] = temp;
			x = rc(x);
		}
	}

	// return the deleted tree
	return heap[hc];
}

// prototype function
static void code(struct tree *n);

// print the encoding
static void printEncoding()
{
	int c;
	for (c = 0; c < 257; c++){
		if(nodes[c] != NULL){
			if(isprint(c))
				printf("'%c' ", c);
			else
				printf("%d ");
			printf(": %o ", getData(nodes[c]));
			code(nodes[c]);
			printf("\n");
		}
	}
}

// writes out the encoding for each character
static void code(struct tree *n)
{
	if(getParent(n) == NULL)
		return;
	code(getParent(n));
	putchar((n == getLeft(getParent(n)))? '0':'1');
}

int main(int argc, char*argv[])
{
	// check if there were two arguments given
	if(argc < 3)
	{
		printf("Some of the filenames are missing.\n");
		return 0;
	}

	// read the file in the argv and count
	FILE *fp;
	fp = fopen(argv[1], "r");
	int c, j;
	int counts[256]; 
	
	// initialize all of the counts to 0	
	for (j = 0; j < 256; j++)
		counts[j] = 0;

	while((c = getc(fp)) != EOF){
		counts[c]++;
	}
	
	// create subtrees from the data
	for(c = 0; c<256;c++)
		if(counts[c] != 0){
			nodes[c] = createTree(counts[c], NULL, NULL);
			/*insert into priority queue*/
			insert(nodes[c]);
		}else
			nodes[c] = NULL;
	nodes[256] = createTree(0, NULL, NULL);
	insert(nodes[256]);

	// continuously delete the two smallest subtrees and put them back on
	// the priority queue.
	struct tree *n;
	int i = 0;
	while(hc > 1){
		struct tree* n1 = delete();
		struct tree* n2 = delete();
		n = createTree(getData(n1) + getData(n2), n1, n2);
		insert(n);
	}
	printEncoding();
	
	// pack the file and save it to the second filename
	pack(argv[1], argv[2], nodes);
}
