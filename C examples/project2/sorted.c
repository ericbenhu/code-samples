// Eric Hu
// 4/20/11
// Lab 2
// Coding for sorted array implementation

// include header files
#include<stdio.h>
#include<string.h>
#include"set.h"
#include<assert.h>
#include<stdbool.h>

// structures:
struct set{
	int count;
	int length;
	char **elts;
};

//implementations of functions:

// creating a set
// algorithmic complexity: O(1)
struct set *createSet(int maxElts)
{
	struct set *sp; /*create a pointer to a set to allocate memory */
	sp = (struct set*)malloc(sizeof(struct set));
	assert(sp!=NULL); /*check if the memory could be allocated */
	
	// initialize default components of the set
	sp->count = 0;
	sp->length = maxElts;
	sp->elts = (char**)malloc(sizeof(char*) * maxElts);/*allocate memory*/
	assert(sp->elts != NULL);/*check if the memory could be allocated*/
	return sp;
}

// destroying a set
// algorithmic complexity: O(n)
void destroySet(struct set *sp)
{
	int i;
	for(i = 0; i < sp->count; i++)
		free(sp->elts[i]);
	free(sp->elts);
	free(sp);
	return;
}

// return number of elements
// algorithmic complexity: O(1)
int numElements(struct set *sp)
{
	return sp->count;
}

// a static function that will find an element and return the index
// If the function didn't find the element, it can return the index of where
// to add the element.
// algorithmic complexity: O(logn)
static int findElement(struct set *sp, char*elt, int count, bool *found)
{
	int lo, hi, mid;
	lo = 0;
	hi = count - 1;
	while(lo <= hi){
		mid = (hi + lo) / 2;
		if(strcmp(elt, sp->elts[mid]) < 0)
			hi = mid - 1;
		else if(strcmp(elt, sp->elts[mid]) > 0)
			lo = mid + 1;
		else{
			*found = true;
			return mid;
		}
	}
	// lo indicates where the index should go
	*found = false;
	return lo;
}

// search for the existence of an element using binary search
// algorithmic complexity: O(logn)
bool hasElement(struct set *sp, char *elt)
{
	// reuse the findElement function to simplify code 
	bool found;
	findElement(sp,elt,sp->count, &found);
	return (found);
}

// add an element to the set pointed to by sp and return whether it changed
// algorithmic complexity: O(n)
bool addElement(struct set *sp, char *elt)
{
	// kill the program before things go wrong using assert
	assert(sp!= NULL);
	assert(sp->count < sp->length);
	
	// find the index to insert the element if it's not already there
	bool found;
	int idx = findElement(sp, elt, sp->count, &found);
	// end the function if the element already exists
	if (found)
		return false;

	int i;
	for(i = sp->count; i > idx; i--){
		sp->elts[i] = sp->elts[i - 1];
	}
	sp->elts[idx] = strdup(elt);
	sp->count++;
	return true;
}

// remove an element from the set pointed to by sp and return whether it changed
// algorithmic complexity: O(n)
bool removeElement(struct set *sp, char *elt)
{
	bool found;
	
	// reuse the findElement function to simplify the search
	int idx = findElement(sp, elt, sp->count, &found);

	// remove the element by shifting elments down into the space
	// from the right (starting from the left)
	int i;
	free(sp->elts[idx]);
	for (i = idx + 1; i < sp->count ; i++)
		sp->elts[i - 1] = sp->elts[i];
	sp->count--;
}
