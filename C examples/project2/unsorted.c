// Eric Hu
// Coen 12
// Lab 2

// Implementation using an unsorted list: Use sequential search to find stuff

// include header files
#include<stdio.h>
#include<string.h>
#include"set.h"
#include<assert.h>
#include<stdbool.h>

// structures:
struct set{
	int count;
	int length;
	char **elts;
};

// implementations of functions:

// creating a set
// algorithmic complexity: O(1)
struct set *createSet(int maxElts)
{
	struct set *sp; /* create a pointer to a set to allocate memory */
	sp = (struct set*)malloc(sizeof(struct set));
	assert(sp!=NULL); /*check if the memory could be allocated */

	// initialize default components of the set
	sp->count = 0;
	sp->length = maxElts;
	sp->elts = (char**)malloc(sizeof(char*) * maxElts);/*allocate memory*/
	assert(sp->elts != NULL);/*check if the memory could be allocated */
	return sp;
}

// destroying a set
// algorithmic complexity: O(n)
void destroySet(struct set *sp)
{
	int i;
	for(i = 0; i < sp->count; i++)
		free(sp->elts[i]);
	free(sp->elts);
	free(sp);
	return;
}

// return number of elements
// algorithmic complexity: O(1)
int numElements(struct set *sp)
{
	return sp->count;
}

// a static function that will find an element and return the index
// algorithmic complexity: O(n)
static int findElement(struct set *sp, char*elt)
{
	int i;
	for(i = 0; i< sp->count; i++)
		if (strcmp(sp->elts[i],elt)==0)
			return i;
	return -1;
}

// search for the existence of an element using sequential search
// algorithmic complexity: O(n)
bool hasElement(struct set *sp, char *elt)
{
	// reuse the findElement function to simplify code
	return(findElement(sp,elt) != -1);
}

// add an element to the set pointed to by sp and return whether it changed
// algorithmic complexity: O(n)
bool addElement(struct set *sp, char *elt)
{
	// kill the program before things go wrong using assert
	assert(sp!=NULL);
	assert(sp->count < sp->length);
	if (hasElement(sp,elt))
		return false;
	sp->elts[sp->count] = strdup(elt);
	sp->count++;
	return true;
}

// remove an element from the set pointed to by sp and return whether it changed
// algorithmic complexity: O(n)
bool removeElement(struct set *sp, char *elt)
{
	// reuse the findElement function to simplify the search
	int i = findElement(sp, elt);
	if (i == -1)
		return false;
	
	// remove the element by overwriting it with the last element in the 
	// array of elements`

	free(sp->elts[i]);
	sp->elts[i] = sp->elts[sp->count -1];
	sp->count--;
	return true;

	// there is no need to set the last element to anything because it will
	// be overwritten later and never accessed until then
}
