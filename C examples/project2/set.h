// Eric Hu
// Coen 12
// Lab 2

// header file: contains all of the functions awaiting implementation

#include<stdbool.h>

// functions:
struct set *createSet(int maxElts);
void destroySet(struct set *sp);
int numElements(struct set *sp);
bool hasElement(struct set *sp, char *elt);
bool addElement(struct set *sp, char *elt);
bool removeElement(struct set *sp, char *elt);
