// Eric Hu
// 3/30/11
// Project 1

// The goal of this lab is to read from a large text file and count the number
// of total words and unique words.
#include<stdio.h>
#include<string.h>

#define NODE struct node
#define MAX_UNIQUE 18000
#define MAX_WORD_LENGTH 30

// Use a linked list to store the unique words to be accessed later.
struct node
{
	char word[MAX_WORD_LENGTH];
	NODE *next;
	NODE *prev;
};

// global variables

// pointers for the head and tail of the list
NODE *head = (NODE*)NULL;
NODE *tail = (NODE*)NULL;

// counters
int word_count = 0;/* total words */
int u_count = 0;   /* unique words */

// insert into the linked list
void 
insert(char* word)
{
	// create a temporary node to insert
	NODE *temp;
	temp = (NODE*)malloc(sizeof(NODE));

	// check if there is enough memory to create a temp
	if (temp == (NODE*)NULL)
	{
		printf("Not enough memory.\n");
		return;
	}
	
	// initialize the temporary node
	strcpy(temp->word, word);

	//empty case
	if (head == (NODE*)NULL)
	{
		temp->next = (NODE*)NULL;
		temp->prev = (NODE*)NULL;
		head = tail = temp;
	}
	
	// all other cases, add to the end of the list
	else
	{
		temp->next = (NODE*)NULL;
		temp->prev = tail;
		tail->next = temp;
		tail = temp;
	}

	// increment the u_count for unique words everytime a node is added to
	// the list
	u_count++;

	return;
}

// function to read the file and count how many total words and unique words
void 
read_file(char *file_name)
{
	// open the file
	FILE *fp;
	fp = fopen(file_name,"r");
	int ret;
	char word[MAX_WORD_LENGTH];
	
	// check if the file was opened
	if (fp == (FILE*)NULL)
	{
		printf("No file\n");
		return;
	}

	// keep reading until finished
	while (1)
	{
		ret = fscanf(fp,"%s", word);
		if (ret <1) /* read a word if ret was only 1*/
			/* anything less would be an error or finish */
		{
			fclose(fp);
			return;
		}

		// increment the total word count
		word_count++;

		// search the list of words for the word that was read
		if (head == (NODE*)NULL)
			insert(word);
		else
		{
			NODE *p;
			p = head;
			while(p != (NODE*)NULL)
			{
				if (strcmp(p->word, word) == 0)
					break;
				p = p->next;
			}
			// if the word was not found in the list, add the 
			// word to the list
			if (p == (NODE*)NULL)
				insert(word);
		}
	}
}

// main code
int main(int argc, char*argv[])
{
	// check if there is one argument given
	if (argc == 1)
	{
		printf("The name of the file is missing,\n");
		return 0;
	}

	// read the file in the argv
	else
	{
		read_file (argv[1]);
		printf("%d total words\n%d distinct words\n", 
			word_count, u_count);
	}
}
