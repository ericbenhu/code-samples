// Eric Hu
// 5/12/11
// Coen 12
// Lab 4

// deque implementation

// header files
#include <stdio.h>
#include "deque.h"
#include <assert.h>

// define structures:
struct node{
	int data;
	struct node *next;
	struct node *prev;
};

struct deque{
	int count;
	struct node *head; /* use the head as the dummy node where the 
			      first node is head->next and the last node
			      is head->prev */
};

// create a deque
// algorithmic complexity: O(1)
struct deque *createDeque(void)
{
	struct deque *dp; /*create a pointer to allocate memory to*/
	dp = (struct deque*)malloc(sizeof(struct deque));
	assert(dp != NULL); /*check if the memory could be allocated */

	// initialize defaults of the deque
	dp->count = 0;

	struct node *head;
	head = (struct node*)malloc(sizeof(struct node));
	assert(head != NULL);
	dp->head = head;
	dp->head->next = dp->head;
	dp->head->prev = dp->head;
	
	/* the head points itself initially because it is a circular list */
	// return the deque
	return dp;
}

// destroy a deque
// algorithmic complexity: O(n)
void destroyDeque(struct deque *dp)
{
	// call the removeLast function to remove elements from the deque until
 	// there are no elements left except the dummy node (head)
	struct node *p, *temp;
	p = dp->head;
	do{
		temp = p;
		p = p->next;
		free(temp);
	}while(p!=dp->head);

	// once all of the nodes are removed, free the memory of the head
	// check if dp->head is NULL just in case the loop above removed the 
	// head from the start
	if(dp->head != NULL)
		free(dp->head);

	// free the memory of the deque
	free(dp);

	// return
	return;
}

// number of items
// algorithmic complexity: O(1)
int numItems(struct deque *dp)
{
	// return the count of dp
	return dp->count;
}

// create a node
// algorithmic complexity: O(1)
static struct node *createNode(int data)
{
	// allocate memory for a new node
	struct node *pNew = (struct node*)malloc(sizeof(struct node));

	// initialize the node
	pNew->data = data;

	// return the newly allocated node
	return pNew;
}

// destroy a node and deallocate the memory
// algorithmic complexity: O(1)
static int destroyNode(struct node *pDel)
{
	// remember the data from the node being deleted
	int data = pDel->data;

	// remove the node by first moving the pointers from its next and prev
	pDel->prev->next = pDel->next;
	pDel->next->prev = pDel->prev;

	// free the memory
	free(pDel);
	
	return data;
}

// add a node to the "front" of the list
// algorithmic complexity: O(1)
void addFirst(struct deque *dp, int x)
{
	// create a new node
	struct node *pNew;
	pNew = createNode(x);

	// link the new node to the list
	pNew->prev = dp->head; 
	pNew->next = dp->head->next;
	pNew->next->prev = pNew;
	pNew->prev->next = pNew;
	dp->count++;	

	return;
}

// add a node to the "back" of the list
// algorithmic complexity: O(1)
void addLast(struct deque *dp, int x)
{
	// create a new node
	struct node *pNew;
	pNew = createNode(x);
	
	// link the new node to the list
	pNew->prev = dp->head->prev;
	pNew->next = dp->head;
	pNew->next->prev = pNew;
	pNew->prev->next = pNew;
	dp->count++;
	
	return;
}

// remove the first node from the list
// algorithmic complexity: O(1)
int removeFirst(struct deque *dp)
{
	if (dp->count <=0)
		return -1;
	// check if the first node of the list is not itself first
	//assert(dp->head->next != dp->head);
	dp->count--;
	
	// the first node of the list is head->next
	return destroyNode(dp->head->next);
}

// remove the last node from the list
// algorithmic complexity: O(1)
int removeLast(struct deque *dp)
{
	if (dp->count <=0)
		return -1;
	// check if the last node of the list is not itself first
	//assert(dp->head->prev != dp->head);
	dp->count--;
	
	// the last node of the list is head->prev
	return destroyNode(dp->head->prev);
}

// get the data of the first node of the list
int getFirst(struct deque *dp)
{
	// check if the list is "empty" or not
	//assert(dp->head->next != dp->head);

	// return the data from head->next
	return dp->head->next->data;
}

// get the data of the last node of the list
int getLast(struct deque *dp)
{
	// check if the list is "empty" or not
	//assert(dp->head->prev != dp->head);

	// return the data from head->prev
	return dp->head->prev->data;
}
