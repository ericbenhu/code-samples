/*
 * File:        deque.h
 *
 * Description: This file contains the public function declarations for a
 *              deque abstract data type for integers.  A deque is a
 *              doubly-ended queue, in which items can only be added to or
 *              removed from the front or rear of the list.
 */

extern struct deque *createDeque(void);

extern void destroyDeque(struct deque *dp);

extern int numItems(struct deque *dp);

extern void addFirst(struct deque *dp, int x);

extern void addLast(struct deque *dp, int x);

extern int removeFirst(struct deque *dp);

extern int removeLast(struct deque *dp);

extern int getFirst(struct deque *dp);

extern int getLast(struct deque *dp);
