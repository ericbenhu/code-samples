// Eric Hu

// radix sort implementation

// header files:
#include <stdio.h>
#include <assert.h>
#include "deque.h"

// create an array of 10 deques to put each number into
int main(void)
{
	// create a main queue to list all of the numbers from standard input
	struct deque *dp;
	dp =  createDeque();
	
	// create an array of 10 deques to put each number into while sorting
	struct deque *queues[10];
	for(int i = 0; i< 10; i++)
		queues[i] = createDeque();
	
	// keep track of the number scanned from the standard input
	int num, max = 0;
	char buf[10000];
		
	// read number from the standard input and store into the main queue
	// while checking for the maximum number
	while (fgets(buf, sizeof(buf), stdin) != NULL){
		num = atoi(buf);
		if(max < num)
			max = num;
		// add to the deque
		addLast(dp, num);
	}
	
	// divisor will multiply by 10 each iteration
	int divisor = 1;
	
	do{
		while(numItems(dp) > 0)
		{
			// sort by taking the first value and dividing it by the 
			// divisor, then modding it by 10
			int data = removeFirst(dp);
			addLast(queues[data / divisor % 10], data);
		}
		
		// add the data from all of the queues back into the list
		for(int i = 0; i < 10; i++)
			while(numItems(queues[i]) > 0)
				addLast(dp, removeFirst(queues[i]));
		divisor *= 10; 
	}while(divisor < max);
	
	printf("Sorted numbers:\n");
	// print the sorted data from the list
	while(numItems(dp))
		printf("%d\n", removeFirst(dp));

	// destroy the queues
	destroyDeque(dp);
	for(int i = 0; i< 10;i++)
		destroyDeque(queues[i]);
}
