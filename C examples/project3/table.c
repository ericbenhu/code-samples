// Eric Hu
// 4/27/11
// Lab 3

// Implementation of a hash table.

// include header files
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "set.h"
#include <assert.h> 

// define flag representations of characters
#define EMPTY 'e'
#define FILLED 'f'
#define DELETED 'd'

// structures:
struct set{
	int count;
	int length;
	char **elts;
	char *flags; /*keeps track of tombstones for hash table*/
};

/* create a set
algorithmic complexity: O(m) where m is the length*/
struct set *createSet(int maxElts)
{
	struct set *sp; /* create a pointer to allocate memory */
	sp = (struct set*)malloc(sizeof(struct set));
	assert(sp!=NULL); /*check if the memory could be allocated */
	

	// initialize default componenets of the set
	sp->count = 0;
	sp->length = maxElts;
	sp->elts = (char**)malloc(sizeof(char*) * sp->length); /*allocate memory*/
	assert(sp->elts != NULL); /*check if the memory could be allocated*/
	sp->flags = (char*)malloc(sizeof(char) * sp->length); /*allocate memory*/
	assert(sp->flags != NULL); /*check if the memory culd be allocated*/
	
	// initialize all flags to empty
	int i;
	for (i = 0; i < sp->length; i++)
		sp->flags[i] = EMPTY;
	
	// return the pointer to the set
	return sp;
}

/* destroying a set
algorithmic complexity: O(m) where m is the length*/
void destroySet(struct set *sp)
{
	// free each element that is there
	int i;
	for(i = 0; i<sp->length;i++)
		if(sp->flags[i] == FILLED)
			free(sp->elts[i]);

	// free the array of elements itself
	free(sp->elts);
	
	// free the flags too by eliminating the pointer to the flags
	free(sp->flags);

	// free the set
	free(sp);
}

// return the number of elements be returning the count
// algorithmic complexity: O(1)
int numElements(struct set *sp)
{
	// simply return the count of the set
	return sp->count;
}

/* hash function */
// algorithmic complexity: O(length of s)
static unsigned hashString(char*s)
{
	unsigned hash = 0;
	while(*s != '\0')
		hash = 31 * hash + *s ++;
	
	// return the hashed value
	return hash;
}

/* find and return the element's location or if the element isn't there, return
the location it should go to
On average, algorithmic complexity: O(1)/worst case O(n)*/
static int findElement(struct set *sp, char *elt, bool *found)
{
	/* keep track of the hashed index and modulus the value to keep 
	   within bounds */
	unsigned hash = hashString(elt) % sp->length;
	
	// create an index at the hashed value
	int i = hash;

	/* also keep track of the last index that was deleted if possible to 
	   insert into that location later if an element was not found */
	int didx = -1;

	/* first check if the slot is empty because if it is, that means that
	   the element isn't in the set. Then check if the slot is filled so
	   that the function can check the value */
	do
	{
		// check if empty
		if(sp->flags[i] == EMPTY)
		{
			// set found to false
			*found = false;
			if(didx != -1)
				// return the last deleted index
				return didx;

			// return the current index
			return i;
		}
		// check if the slot is filled
		if(sp->flags[i] == FILLED)
			if(strcmp(sp->elts[i],elt)==0)
			{
				// set found to true
				*found = true;
				
				// return the current index where it found it
				return i;
			}
		/* if slot is deleted, the function remembers that spot 
			and the function continues to iterate */
		if(sp->flags[i] == DELETED && didx == -1)
			didx = i;
		i++; /*probe*/
		if (i > sp->length);
			i = i % sp->length;
	}while(i != hash);
	
	// if the function managed to iterate through the entire list and not 
	// return, then the element is obviously not in the list nor can it be
	// inserted
	*found = false;
	
	// return an invalid index
	return -1;
}

// search for an element using hashing
// algorithmic complexity: O(1)/O(n)
bool hasElement(struct set *sp, char *elt)
{
	// reuse findElement function to simplify code
	bool found;
	findElement(sp,elt, &found);
	return(found);
}

// add an element to the set
// algorithmic complexity: O(1)/O(n)
bool addElement(struct set *sp, char *elt)
{
	// kill the program before things go wrong using assert
	assert(sp!=NULL);
	assert(sp->count < sp->length);

	// find the index to insert the element if it's not already there
	bool found;
	int idx = findElement(sp, elt, &found);

	// if the element is already there, return
	if(found)
		return false;
	
	// finally, insert into the table
	sp->elts[idx] = strdup(elt);
	
	// set the value in the parallel array to filled
	sp->flags[idx] = FILLED;
	
	// increment count
	sp->count++;
	return true;
}

// remove an element from the set
// algorithmic complexity: O(1)/O(n)
bool removeElement(struct set *sp, char *elt)
{
	// keep track of the bool return of the findElement
	bool found;

	// reuse the findElement function to simplify the seartch
	int idx = findElement(sp, elt, &found);
	
	// if the element isn't there, return
	if (!found)
		return false;
	
	// remove the element from the index
	free(sp->elts[idx]);

	// decrement the cound
	sp->count--;

	// set the index in the parallel array to deleted
	sp->flags[idx] = DELETED;
	return true;
}
