unique
------
                              unsorted  sorted  hashing
GreenEggsAndHam.txt             .008    .007    .009
Macbeth.txt                     .623    .100    .024
Genesis.txt                     .634    .085    .035
HoundOfTheBaskervilles.txt      2.069   .246    .055
TheWarOfTheWorlds.txt           2.859   .349    .056
TreasureIsland.txt              2.502   .292    .060
TheSecretGarden.txt             2.248   .241    .071
TheCountOfMonteCristo.txt       32.021  2.153   .424
TwentyThousandLeagues.txt       5.995   .586    .092
Bible.txt                       35.169  1.972   .592


bincount
--------
                              unsorted  sorted  hashing
GreenEggsAndHam.txt             .009     .008    .020
Macbeth.txt                     .113     .038    .037
Genesis.txt                     .131     .049    .050
HoundOfTheBaskervilles.txt      .378     .092    .067
TheWarOfTheWorlds.txt           .418     .104    .069
TreasureIsland.txt              .420     .108    .074
TheSecretGarden.txt             .395     .112    .082
TheCountOfMonteCristo.txt       4.634    .718    .381
TwentyThousandLeagues.txt       .817     .175    .104
Bible.txt                       5.533    .973    .605


parity
------
                              unsorted  sorted  hashing
GreenEggsAndHam.txt             .009      .008    .009
Macbeth.txt                     2.249     .380    .031
Genesis.txt                     2.766     .420    .050
HoundOfTheBaskervilles.txt      11.292    1.619   .081
TheWarOfTheWorlds.txt           13.759    1.934   .086
TreasureIsland.txt              13.905    1,979   .092
TheSecretGarden.txt             12.416    1.959   .106
TheCountOfMonteCristo.txt       4m47.991  33.205  .901
TwentyThousandLeagues.txt       33.573    4.125   .145
Bible.txt                       5m15.942  32.004  1.153
